//
//  AppConstants.h
//  IWasHere
//
//  Created by Qaiser Abbas on 21/10/2015.
//  Copyright © 2015 BirAlSabia. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h


#endif /* AppConstants_h */


#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

/************************************/
/*            APP USER          */
/************************************/

#define     App_User_ID                                 @"id"
#define     App_User_Email                              @"email"

#define     App_User_PhoneNumber                        @"phone_number"
#define     App_User_UserType                           @"user_type"
#define     App_User_FirstName                          @"first_name"
#define     App_User_LastName                           @"last_name"
#define     App_User_Profile_Pic_Path                   @"profile_url"



/************************************/
/*            Webservices           */
//**********************************

#define     App_BaseUrl                     @""

#define Webservice_AllCities                @""


/************************************/
/*            Date Formates         */
/************************************/

#define     AppDateFormat                @"EEEE, MMMM d"
#define     AppUIDateFormat              @"MM/dd/yy"
#define     AppSignUpDateFormat          @"YYYY-MM-dd"


#define     App_firstTime_Use            @"FirstTime_Use"
#define     App_firstTime_Add            @"FirstTime_Add"


