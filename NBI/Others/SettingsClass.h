//
//  SettingsClass.h
//  MyRentalLog
//
//  Created by JunaidAkram on 2/3/14.
//  Copyright (c) 2014 swenggco-software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface SettingsClass : NSObject


+(BOOL)ios7;
+(BOOL)ios8;


+(BOOL)isiPhone;
+(BOOL)isiPhone5;
+(BOOL)isiPhone6;
+(BOOL)isiPhone6Plus;
+(BOOL)isiPhone4;

+(BOOL)isiPAD;

+(void)setCenterWithRexpectToSuperview:(float)superviewWidth subviewWidth:(float)subviewWidth sibview:(UIView*)view;

+(void)setXAxis:(UIView*)view float:(float)value;

+(void)setYAxis:(UIView*)view float:(float)value;

+(void)setXAxistozero:(UIView*)view;

+(void)setxAxisIfXvalue:(float)value float:(float)requiredvalue view:(UIView*)view;

+(void)setWidthIfXvalue:(float)value float:(float)requiredWidth view:(UIView*)view;

+(void)setWidth:(float)width view:(UIView*)view;

+(void)setHeight:(float)height view:(UIView*)view;

+(void)setchangeInHeight:(UIView*)view float:(float)value;

+(void)setXAxisToPoint:(UIView*)view float:(float)value;



#pragma --
#pragma -- Image Manipulation
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToHeigh: (float) i_height;
+(UIImage *)imagefromVideo :(NSURL*)url;
+ (UIImage *)imageWithSquareImage:(UIImage *)image;
+ (UIImage *)imageWithImageHardCoded:(UIImage *)image size:(CGSize)sizetoscale;
+ (UIImage*)previewFromFileAtPath:(NSString*)path ratio:(CGFloat)ratio;

#pragma mark --
#pragma mark Email Validation
+(BOOL)isValidEmail:(NSString*)email;


#pragma mark --
#pragma mark iCloud Data storage chnage

+(void)removeFileFromBackup:(NSString*)directory;

+ (UIImage*)compressImage:(UIImage*)OriginaleImage;
+ (UIImage*) rotateImageAppropriately:(UIImage*)imageToRotate;
+ (UIImage *)imageRotatedByDegrees:(CGFloat)degrees imageRotation:(UIImage*)imageToRotate;


+(void)saveUser:(NSMutableDictionary*)dataDict rememberMe:(BOOL)rememberMe;

+(NSString*)decodeBase64String:(NSString*)string;
+(NSString*)encodeStringTo64:(NSString*)fromString;

+(NSString *)auth_key;
+(void)setAuth_key:(NSString *)auth_key;
+(void)RemoveAuth_key;

@end
