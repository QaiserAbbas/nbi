//
//  DataManager.m
//  VisitorBook
//
//  Created by Zykov on 02.12.14.
//  Copyright (c) 2014 Zykov. All rights reserved.
//

#import "DataManager.h"
#import "SingletonClass.h"
#import "SettingsClass.h"

@interface DataManager ()

@end


@implementation DataManager


+ (DataManager *) sharedManager
{
    static DataManager * manager;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DataManager alloc] init];
    });
    
    return manager;
}



#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationURLDocumentsDirectory
{
    // The directory the application uses to store the Core Data store file. This code uses a directory named "MS.VisitorBook" in the application's documents directory.
    return [NSURL fileURLWithPath:[self applicationDocumentsDirectory]];
}

- (NSString *)applicationDocumentsDirectory
{
    // The directory the application uses to store the Core Data store file. This code uses a directory named "MS.VisitorBook" in the application's documents directory.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    return documentsDirectory;
}

- (NSManagedObjectModel *)managedObjectModel
{
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BeRain" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationURLDocumentsDirectory] URLByAppendingPathComponent:@"BeRain.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
            if (error!=nil) {
                NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
            }
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
            dict[NSLocalizedFailureReasonErrorKey] = failureReason;
            dict[NSUnderlyingErrorKey] = error;
            error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
#ifdef DEBUG
            abort();
#endif
            
        }
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext
{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext
{
    
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    if (managedObjectContext != nil) {
        NSError *error = nil;
        [managedObjectContext save:&error];
        
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
//            #ifdef DEBUG
//            abort();
//            #endif
        }
    }
}

- (BOOL)dropDataStore
{

    NSError *error = nil;
    NSArray *stores = [self.persistentStoreCoordinator persistentStores];
    
    for(NSPersistentStore *store in stores) {
        [self.persistentStoreCoordinator removePersistentStore:store error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:&error];
        if (error!=nil) {
            NSLog(@"%s[%d:%s]: %@", __FILE__, __LINE__, __FUNCTION__,error.localizedDescription);
        }
    }
    
    _persistentStoreCoordinator = nil;
    _managedObjectContext = nil;
    _managedObjectModel = nil;
    
    return YES;
}


-(void)saveDataToModul:(NSMutableDictionary*)modul1Dict modulNumber:(NSString*)number folderName:(NSString*)folder;
{
    NSString *plistPath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/Modul%@.plist",folder,number]];
    [modul1Dict writeToFile:plistPath atomically:YES];
}
-(void)movePlistsToDocumentDirectories:(NSString*)folderNumber
{
    NSFileManager *fileManger=[NSFileManager defaultManager];
    NSError *error;
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    
    NSString *doumentDirectoryPath=[pathsArray objectAtIndex:0];
    
    NSString *folderPath= [doumentDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",folderNumber]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder

    for (int i=1; i<7; i++)
    {
        NSString *plistName=[NSString stringWithFormat:@"Modul%d.plist",i];
        
        NSString *destinationPath= [NSString stringWithFormat:@"%@/%@",folderPath,plistName];
        
        NSLog(@"plist path %@",destinationPath);
        if ([fileManger fileExistsAtPath:destinationPath]){
            //NSLog(@"database localtion %@",destinationPath);
            return;
        }
        NSString *sourcePath=[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:[NSString stringWithFormat:@"Modul%d.plist",i]];
        
        [fileManger copyItemAtPath:sourcePath toPath:destinationPath error:&error];
    }
}
-(void)saveTest:(NSArray*)nameArray;
{
    NSString *plistPath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:[NSString stringWithFormat:@"SavedTest.plist"]];
    [nameArray writeToFile:plistPath atomically:YES];
}
-(void)removeTestFolder:(NSString*)name;
{
    
    NSFileManager *fileManger=[NSFileManager defaultManager];
    NSError *error;
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    
    NSString *doumentDirectoryPath=[pathsArray objectAtIndex:0];
    
        NSString *destinationPath= [doumentDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",name]];
        
        NSLog(@"plist path %@",destinationPath);
        if ([fileManger fileExistsAtPath:destinationPath]){
            //NSLog(@"database localtion %@",destinationPath);
            [fileManger removeItemAtPath:destinationPath error:&error];
        }
}

-(void)eraseDataPlistsToDocumentDirectories
{
    NSFileManager *fileManger=[NSFileManager defaultManager];
    NSError *error;
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    
    NSString *doumentDirectoryPath=[pathsArray objectAtIndex:0];
    
    for (int i=1; i<7; i++)
    {
        NSString *destinationPath= [doumentDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"Modul%d.plist",i]];
        
        NSLog(@"plist path %@",destinationPath);
        if ([fileManger fileExistsAtPath:destinationPath]){
            //NSLog(@"database localtion %@",destinationPath);
            [fileManger removeItemAtPath:destinationPath error:&error];
        }
        NSString *sourcePath=[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:[NSString stringWithFormat:@"Modul%d.plist",i]];
        
        [fileManger copyItemAtPath:sourcePath toPath:destinationPath error:&error];
    }
    
}
-(NSString*)calculateScore:(NSMutableArray*)questionArray moduleNumber:(int)moduleNumber finalScore:(BOOL)yes;
{
    if (moduleNumber==1)
    {
        return [self calculateTotalNumberChoosenModule1:questionArray final:yes];
    }
    else if (moduleNumber==2)
    {
        return [self calculateTotalNumberChoosenModule2:questionArray final:yes];
    }
    else if (moduleNumber==3)
    {
        return [self calculateTotalNumberChoosenModule3:questionArray final:yes];
    }
    else if (moduleNumber==4)
    {
        return [self calculateTotalNumberChoosenModule4:questionArray final:yes];
    }
    else if (moduleNumber==5)
    {
        return [self calculateTotalNumberChoosenModule5:questionArray final:yes];
    }
    else if (moduleNumber==6)
    {
        return [self calculateTotalNumberChoosenModule6:questionArray final:yes];
    }


    return @"";
}

-(NSString*)calculateTotalNumberChoosenModule1:(NSMutableArray*)questionArray final:(BOOL)yes
{
    NSString *returningStr=@" von 10.0";
    if (yes)
    {
        returningStr=@"";
    }
    int totalNumberChoosen=0;
    for (NSMutableDictionary*dataDict in questionArray)
    {
        if ([dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"] && yes)
        {
            return @"-";
        }
        totalNumberChoosen += [dataDict[@"savedNumber"] intValue];
    }
    
    if (totalNumberChoosen>=0 && totalNumberChoosen<=1)
    {
        return [NSString stringWithFormat:@"0.0%@",returningStr];
    }
    else if (totalNumberChoosen>=2 && totalNumberChoosen<=3)
    {
        return [NSString stringWithFormat:@"2.5%@",returningStr];
    }
    else if (totalNumberChoosen>=4 && totalNumberChoosen<=5)
    {
        return [NSString stringWithFormat:@"5.0%@",returningStr];
    }
    else if (totalNumberChoosen>=6 && totalNumberChoosen<=9)
    {
        return [NSString stringWithFormat:@"7.5%@",returningStr];
    }
    else if (totalNumberChoosen>=10)
    {
        return [NSString stringWithFormat:@"10.0%@",returningStr];
    }
    return [NSString stringWithFormat:@"0.0%@",returningStr];
}

-(NSString*)calculateTotalNumberChoosenModule2:(NSMutableArray*)questionArray final:(BOOL)yes
{
    NSString *returningStr=@" von 15.0";
    if (yes)
    {
        returningStr=@"";
    }

    int totalNumberChoosen=0;
    for (NSMutableDictionary*dataDict in questionArray)
    {
        if ([dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"] && yes)
        {
            return @"-";
        }

        {
            totalNumberChoosen += [dataDict[@"savedNumber"] intValue];
        }
    }
    
    if (totalNumberChoosen>=0 && totalNumberChoosen<=1)
    {
        return [NSString stringWithFormat:@"0.0%@",returningStr];
    }
    else if (totalNumberChoosen>=2 && totalNumberChoosen<=5)
    {
        return [NSString stringWithFormat:@"3.75%@",returningStr];
    }
    else if (totalNumberChoosen>=6 && totalNumberChoosen<=10)
    {
        return [NSString stringWithFormat:@"7.5%@",returningStr];
    }
    else if (totalNumberChoosen>=11 && totalNumberChoosen<=16)
    {
        return [NSString stringWithFormat:@"11.25%@",returningStr];
    }
    else if (totalNumberChoosen>=17)
    {
        return [NSString stringWithFormat:@"15.0%@",returningStr];
    }
    return [NSString stringWithFormat:@"0.0%@",returningStr];
}

-(NSString*)calculateTotalNumberChoosenModule3:(NSMutableArray*)questionArray final:(BOOL)yes
{
    NSString *returningStr=@" von 15.0";
    if (yes)
    {
        returningStr=@"";
    }

    int totalNumberChoosen=0;
    for (NSMutableDictionary*dataDict in questionArray)
    {
        if ([dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"] && yes)
        {
            return @"-";
        }

        {
            totalNumberChoosen += [dataDict[@"savedNumber"] intValue];
        }
    }
    
    if (totalNumberChoosen==0)
    {
        return [NSString stringWithFormat:@"0.0%@",returningStr];
    }
    else if (totalNumberChoosen>=1 && totalNumberChoosen<=2)
    {
        return [NSString stringWithFormat:@"3.75%@",returningStr];
    }
    else if (totalNumberChoosen>=3 && totalNumberChoosen<=4)
    {
        return [NSString stringWithFormat:@"7.5%@",returningStr];
    }
    else if (totalNumberChoosen>=5 && totalNumberChoosen<=6)
    {
        return [NSString stringWithFormat:@"11.25%@",returningStr];
    }
    else if (totalNumberChoosen>=7)
    {
        return [NSString stringWithFormat:@"15.0%@",returningStr];
    }
    return [NSString stringWithFormat:@"15.0%@",returningStr];
}
-(NSString*)calculateTotalNumberChoosenModule4:(NSMutableArray*)questionArray final:(BOOL)yes
{
    NSString *returningStr=@" von 40.0";
    if (yes)
    {
        returningStr=@"";
    }

    int totalNumberChoosen=0;
    for (NSMutableDictionary*dataDict in questionArray)
    {
        if ([dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"] && yes)
        {
            return @"-";
        }

        {
            totalNumberChoosen += [dataDict[@"savedNumber"] intValue];
        }
    }
    
    if (totalNumberChoosen>=0 && totalNumberChoosen<=2)
    {
        return [NSString stringWithFormat:@"0.0%@",returningStr];
    }
    else if (totalNumberChoosen>=3 && totalNumberChoosen<=7)
    {
        return [NSString stringWithFormat:@"10.0%@",returningStr];
    }
    else if (totalNumberChoosen>=8 && totalNumberChoosen<=18)
    {
        return [NSString stringWithFormat:@"20.0%@",returningStr];
    }
    else if (totalNumberChoosen>=19 && totalNumberChoosen<=36)
    {
        return [NSString stringWithFormat:@"30.0%@",returningStr];
    }
    else if (totalNumberChoosen>=37)
    {
        return [NSString stringWithFormat:@"40.0%@",returningStr];
    }
    return [NSString stringWithFormat:@"0.0%@",returningStr];
}

-(NSString*)calculateTotalNumberChoosenModule6:(NSMutableArray*)questionArray final:(BOOL)yes
{
    NSString *returningStr=@" von 15.0";
    if (yes)
    {
        returningStr=@"";
    }

    int totalNumberChoosen=0;
    for (NSMutableDictionary*dataDict in questionArray)
    {
        if ([dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"] && yes)
        {
            return @"-";
        }

        {
            totalNumberChoosen += [dataDict[@"savedNumber"] intValue];
        }
    }
    
    if (totalNumberChoosen==0)
    {
        return [NSString stringWithFormat:@"0.0%@",returningStr];
    }
    else if (totalNumberChoosen>=1 && totalNumberChoosen<=3)
    {
        return [NSString stringWithFormat:@"3.75%@",returningStr];
    }
    else if (totalNumberChoosen>=4 && totalNumberChoosen<=6)
    {
        return [NSString stringWithFormat:@"7.5%@",returningStr];
    }
    else if (totalNumberChoosen>=7 && totalNumberChoosen<=11)
    {
        return [NSString stringWithFormat:@"11.256%@",returningStr];
    }
    else if (totalNumberChoosen>=12)
    {
        return [NSString stringWithFormat:@"15.0%@",returningStr];
    }
    return [NSString stringWithFormat:@"0.0%@",returningStr];
}
-(NSString*)calculateTotalNumberChoosenModule5:(NSMutableArray*)questionArray final:(BOOL)yes
{
    NSString *returningStr=@" von 20.0";
    if (yes)
    {
        returningStr=@"";
    }

    int totalNumberChoosen=0;
    int savedNumber1=0;
    int savedNumber2=0;
    int savedNumber3=0;
    int savedNumber4=0;
    int savedNumber5=0;
    int savedNumber6=0;

    for (int i=1; i<=questionArray.count; i++)
    {
        NSMutableDictionary*dataDict=[NSMutableDictionary dictionaryWithDictionary:questionArray[i-1]];
        if ([dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"] && yes)
        {
            return @"-";
        }
        
        if (i>=1 && i<=7)
        {
            if ([dataDict[@"savedNumber"] intValue]>savedNumber1)
            {
                savedNumber1=[dataDict[@"savedNumber"] intValue];
            }
        }
        if (i>=8 && i<=11)
        {
            if ([dataDict[@"savedNumber"] intValue]>savedNumber2)
            {
                savedNumber2=[dataDict[@"savedNumber"] intValue];
            }
        }
        if (i==12)
        {
            if ([dataDict[@"savedNumber"] intValue]>savedNumber3)
            {
                savedNumber3=[dataDict[@"savedNumber"] intValue];
            }
        }
        if (i>=13 && i<=14)
        {
            if ([dataDict[@"savedNumber"] intValue]>savedNumber4)
            {
                savedNumber4=[dataDict[@"savedNumber"] intValue];
            }
        }
        if (i==15)
        {
            if ([dataDict[@"savedNumber"] intValue]>savedNumber5)
            {
                savedNumber5=[dataDict[@"savedNumber"] intValue];
            }
        }
        if (i==16)
        {
            if ([dataDict[@"savedNumber"] intValue]>savedNumber6)
            {
                savedNumber6=[dataDict[@"savedNumber"] intValue];
            }
        }



    }
    totalNumberChoosen=savedNumber1+savedNumber2+savedNumber3+savedNumber4+savedNumber5+savedNumber6;
    
//    for (NSMutableDictionary*dataDict in questionArray)
//    {
//        if ([dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"] && yes)
//        {
//            return @"-";
//        }
//
//        {
//            totalNumberChoosen += [dataDict[@"savedNumber"] intValue];
//        }
//    }

    if (totalNumberChoosen==0)
    {
        return [NSString stringWithFormat:@"0.0%@",returningStr];
    }
    else if (totalNumberChoosen>=0 && totalNumberChoosen<=1)
    {
        return [NSString stringWithFormat:@"5.0%@",returningStr];
    }
    else if (totalNumberChoosen>1 && totalNumberChoosen<=3)
    {
        return [NSString stringWithFormat:@"10.0%@",returningStr];
    }
    else if (totalNumberChoosen>=4 && totalNumberChoosen<=5)
    {
        return [NSString stringWithFormat:@"15.0%@",returningStr];
    }
    else if (totalNumberChoosen>=6)
    {
        return [NSString stringWithFormat:@"20.0%@",returningStr];
    }
    return [NSString stringWithFormat:@"0.0%@",returningStr];
}

-(int)calculateMdule5Score:(NSDictionary*)dataDict questionNumber:(int)number questionArray:(NSMutableArray*)questionArray;
{
    if (number>=1 && number<=7)
    {
        double proManat=0;
        double proTag=0;
        double proWoche=0;
        
        for (int i=1; i<=7; i++)
        {
            NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:questionArray[i-1][@"savedValues"]];
            proTag=proTag+[dict[@"proTag"] doubleValue];
            proWoche=proWoche+[dict[@"proWoche"] doubleValue];
            proManat=proManat+[dict[@"proManat"] doubleValue];
        }
        
        double userInput = (proTag + (proWoche/7) + (proManat/30));

//        float userInput = ([dataDict[@"proTag"] floatValue] + ([dataDict[@"proWoche"] floatValue]/7) + ([dataDict[@"proManat"] floatValue]/30));
        if (userInput<1)
        {
            return 0;
        }
        else if (userInput>=1 && userInput<3)
        {
            return 1;
        }
        else if (userInput>=3 && userInput<8)
        {
            return 2;
        }
        else if (userInput>=8)
        {
            return 3;
        }
    }
     else if (number>=8 && number<=11)
    {
        double proManat=0;
        double proTag=0;
        double proWoche=0;
        
        for (int i=8; i<=11; i++)
        {
            NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:questionArray[i-1][@"savedValues"]];
            proTag=proTag+[dict[@"proTag"] doubleValue];
            proWoche=proWoche+[dict[@"proWoche"] doubleValue];
            proManat=proManat+[dict[@"proManat"] doubleValue];
        }

        double userInput = (proTag + (proWoche/7) + (proManat/30));
        if (userInput<0.14)
        {
            return 0;
        }
        else if (userInput>=0.14 && userInput<1)
        {
            return 1;
        }
        else if (userInput>=1 && userInput<3)
        {
            return 2;
        }
        else if (userInput>=3)
        {
            return 3;
        }
    }
     else if (number==12)
     {
         double proManat=0;
         double proTag=0;
         double proWoche=0;
         
         {
             NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:questionArray[11][@"savedValues"]];
             proTag=proTag+[dict[@"proTag"] doubleValue];
             proWoche=proWoche+[dict[@"proWoche"] doubleValue];
             proManat=proManat+[dict[@"proManat"] doubleValue];
         }

         double userInput = (proTag * 60 + (proWoche* 8.6) + (proManat*2));
         if (userInput<4.3)
         {
             return 0;
         }
         else if (userInput>=4.3 && userInput<8.6)
         {
             return 1;
         }
         else if (userInput>=8.6 && userInput<12.9)
         {
             return 2;
         }
         else if (userInput>=12.9 && userInput<60)
         {
             return 3;
         }
         else if (userInput>=60)
         {
             return 6;
         }
     }
     else if (number>=13 && number<=14)
     {
         double proManat=0;
         double proTag=0;
         double proWoche=0;
         
         for (int i=13; i<=14; i++)
         {
             NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:questionArray[i-1][@"savedValues"]];
             proTag=proTag+[dict[@"proTag"] doubleValue];
             proWoche=proWoche+[dict[@"proWoche"] doubleValue];
             proManat=proManat+[dict[@"proManat"] doubleValue];
         }

         double userInput = (proTag * 60 + (proWoche* 4.3) + (proManat));

         if (userInput<4.3)
         {
             return 0;
         }
         else if (userInput>=4.3 && userInput<8.6)
         {
             return 1;
         }
         else if (userInput>=8.6 && userInput<12.9)
         {
             return 2;
         }
         else if (userInput>=12.9 && userInput<60)
         {
             return 3;
         }
         else if (userInput>=60)
         {
             return 6;
         }
     }
     else if (number==15)
     {
         
         double proManat=0;
         double proTag=0;
         double proWoche=0;
         
         {
             NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithDictionary:questionArray[14][@"savedValues"]];
             proTag=proTag+[dict[@"proTag"] doubleValue];
             proWoche=proWoche+[dict[@"proWoche"] doubleValue];
             proManat=proManat+[dict[@"proManat"] doubleValue];
         }
         double userInput = (proTag * 0 + (proWoche* 8.6) + (proManat*2));
         if (userInput<4.3)
         {
             return 0;
         }
         else if (userInput>=4.3 && userInput<8.6)
         {
             return 1;
         }
         else if (userInput>=8.6 && userInput<12.9)
         {
             return 2;
         }
         else if (userInput>=12.9 && userInput<60)
         {
             return 3;
         }
         else if (userInput>=60)
         {
             return 6;
         }
     }



    return 0;
}

-(NSString*)finalPdfScore:(double)score
{
    if (score>=0 && score<=12.5)
    {
        return @"PG 0";
    }
    else if (score>12.5 && score<=27.0)
    {
        return @"PG 1";
    }
    else if (score>27.0 && score<=47.5)
    {
        return @"PG 2";
    }
    else if (score>47.5 && score<=70.0)
    {
        return @"PG 3";
    }
    else if (score>70.0 && score<=90)
    {
        return @"PG 4";
    }
    else if (score>90)
    {
        return @"PG 5";
    }
    return @"-";
}

-(NSString*)calculateTheTotalNumberChoosen:(NSMutableArray*)questionArray final:(BOOL)yes
{
    int totalNumberChoosen=0;
    for (NSMutableDictionary*dataDict in questionArray)
    {
        if ([dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"] && yes)
        {
            return @"-";
        }
        
        {
            totalNumberChoosen += [dataDict[@"savedNumber"] intValue];
        }
    }
    return [NSString stringWithFormat:@"%d",totalNumberChoosen];
}


@end
