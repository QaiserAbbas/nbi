//
//  DataManager.h
//  VisitorBook
//
//  Created by Zykov on 02.12.14.
//  Copyright (c) 2014 Zykov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>



typedef NS_ENUM(NSUInteger, DataManagerPathType) {
    DataManagerPathURL,
    DataManagerPathBody
};


@interface DataManager : NSObject

+ (DataManager *) sharedManager;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSString *)applicationDocumentsDirectory;
- (NSURL *)applicationURLDocumentsDirectory;
- (BOOL)dropDataStore;
-(void)movePlistsToDocumentDirectories:(NSString*)folderNumber;
-(void)eraseDataPlistsToDocumentDirectories;
-(void)removeTestFolder:(NSString*)name;
-(void)saveDataToModul:(NSMutableDictionary*)modul1Dict modulNumber:(NSString*)number folderName:(NSString*)folder;

-(NSString*)calculateScore:(NSMutableArray*)questionArray moduleNumber:(int)moduleNumber finalScore:(BOOL)yes;
-(int)calculateMdule5Score:(NSDictionary*)dataDict questionNumber:(int)number questionArray:(NSMutableArray*)questionArray;
-(NSString*)finalPdfScore:(double)score;
-(NSString*)calculateTheTotalNumberChoosen:(NSMutableArray*)questionArray final:(BOOL)yes;
-(void)saveTest:(NSArray*)nameArray;

@end
