#import "PDFOrderTableHeader.h"

@interface PDFOrderTableHeader ()
@property (weak, nonatomic) IBOutlet UILabel *modulNumber;
@property (weak, nonatomic) IBOutlet UILabel *moduleName;

@end

@implementation PDFOrderTableHeader

- (void)setupWithModul:(NSDictionary*)moduleDict number:(int)number;
{
    [self.moduleName setText:moduleDict[@"title"]];
    [self.modulNumber setText:[NSString stringWithFormat:@"Module%d:",number]];
}
@end
