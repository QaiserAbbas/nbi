#import "PDFOrderTableCell.h"
#import <QuartzCore/QuartzCore.h>


@interface PDFOrderTableCell ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@property (weak, nonatomic) IBOutlet UIImageView *bottomImageView;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UIImageView *sideImageView;
@property (weak, nonatomic) IBOutlet UILabel *totalAnswerLabel;
@end


@implementation PDFOrderTableCell

#pragma mark - Public methods

#pragma mark - Private methods
- (void)setupWithModul:(NSDictionary*)moduleDict;
{
    NSMutableArray*mcqArray=[NSMutableArray arrayWithArray:moduleDict[@"mcqArray"]];
    
    if (mcqArray.count>0)
    {
        [self.questionLabel setText:moduleDict[@"title"]];
        [self.answerLabel setText:moduleDict[@"selected"]];
        [self.totalAnswerLabel setText:moduleDict[@"savedNumber"]];
        [self.totalLabel setText:@""];
        [self.bottomImageView setBackgroundColor:[UIColor darkGrayColor]];
        [self.sideImageView setBackgroundColor:[UIColor darkGrayColor]];

    }
    else
    {
        [self.questionLabel setText:@""];
        [self.answerLabel setText:@""];
        [self.bottomImageView setBackgroundColor:[UIColor clearColor]];
        [self.sideImageView setBackgroundColor:[UIColor clearColor]];

        [self.totalAnswerLabel setText:[NSString stringWithFormat:@"%@",moduleDict[@"totalSelecetd"]]];
        [self.totalLabel setText:moduleDict[@"title"]];

    }
}

@end
