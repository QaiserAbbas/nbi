#import <UIKit/UIKit.h>


@interface PDFOrderTableCellPercentage : UIView

- (void)setupWithModul:(NSDictionary*)moduleDict number:(int)number;

@end
