#import <UIKit/UIKit.h>


@class ORStackView;


@interface PDFOrderView : UIView

@property (strong, atomic) NSString *selectedTest;
@property (nonatomic) NSArray<UIView *> *noWrapViews;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dobLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end
