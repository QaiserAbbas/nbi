#import "PDFOrderTableCellPercentage.h"
#import <QuartzCore/QuartzCore.h>


@interface PDFOrderTableCellPercentage ()
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;
@property (weak, nonatomic) IBOutlet UILabel *totaleSelected;
@property (weak, nonatomic) IBOutlet UILabel *totalCalculated;

@end


@implementation PDFOrderTableCellPercentage

#pragma mark - Public methods

#pragma mark - Private methods
- (void)setupWithModul:(NSDictionary*)moduleDict number:(int)number;
{
    self.totaleSelected.text=moduleDict[@"totalSelecetd"];
    
    if (number==1)
    {
        self.percentLabel.text=@"10%";
        self.totalCalculated.text=[NSString stringWithFormat:@"%@/15",moduleDict[@"total"]];
    }
    else if (number==2)
    {
        self.percentLabel.text=@"15%";
        self.totalCalculated.text=[NSString stringWithFormat:@"%@/15",moduleDict[@"total"]];
    }
    else if (number==3)
    {
        self.percentLabel.text=@"15%";
        self.totalCalculated.text=[NSString stringWithFormat:@"%@/15",moduleDict[@"total"]];
    }
    else if (number==4)
    {
        self.percentLabel.text=@"40%";
        self.totalCalculated.text=[NSString stringWithFormat:@"%@/40",moduleDict[@"total"]];
    }
    else if (number==5)
    {
        self.percentLabel.text=@"20%";
        self.totalCalculated.text=[NSString stringWithFormat:@"%@/20",moduleDict[@"total"]];
    }
    else if (number==6)
    {
        self.percentLabel.text=@"15%";
        self.totalCalculated.text=[NSString stringWithFormat:@"%@/15",moduleDict[@"total"]];
    }
}

@end
