#import "PDFOrderView.h"

// Views
#import "PDFOrderTableCell.h"
#import "PDFOrderTableCellTotalTable.h"
#import "PDFOrderTableCellPercentage.h"
#import "PDFOrderTableCellHeader2.h"

#import "PDFOrderTableHeader.h"

// Libraries
#import <ORStackView/ORStackView.h>
#import <Masonry/Masonry.h>


@interface PDFOrderView ()
@property (weak, nonatomic) IBOutlet UIView *orderTableContainer;
@property (nonatomic) IBOutletCollection(UIView) NSArray *containers;
@property (nonatomic) IBOutletCollection(UIView) NSArray *noWrapOutletCollection;
@property (nonatomic) ORStackView *stackView;
@end


@implementation PDFOrderView

#pragma mark - Awakening

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.selectedTest= [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedTest"];
    [self configureContainerColors];
    [self configureStackView];
    [self configureNoWrapViews];
    NSLog(@"%@",self.selectedTest);
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"SavedTest.plist"]];
    
    // Build the array from the plist
    NSMutableArray *savedTestArray=[NSMutableArray arrayWithContentsOfFile:plistPath];
    NSArray *oldArray=[NSArray arrayWithArray:savedTestArray];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:NO];
    savedTestArray=[NSMutableArray arrayWithArray:[oldArray sortedArrayUsingDescriptors:@[sort]]];
    NSMutableDictionary *selectedTestDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[[self.selectedTest intValue]-1]];
    _nameLabel.text=selectedTestDict[@"name"];
    _dobLabel.text=selectedTestDict[@"dob"];

    _timeLabel.text=selectedTestDict[@"date"];
}

#pragma mark - Configuration

- (void)configureContainerColors {
    for (UIView *container in self.containers) {
        container.backgroundColor = [UIColor whiteColor];
    }
}

- (void)configureStackView {
    self.stackView = [[ORStackView alloc] init];
    self.stackView.backgroundColor = [UIColor whiteColor];
    [self.orderTableContainer addSubview:self.stackView];
    
    [self.stackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.orderTableContainer);
    }];
    for (int i=1; i<7; i++)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0];
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/Modul%d.plist",self.selectedTest ,i]];
        NSMutableDictionary *mainDict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        [self configureOrderTableHeader:mainDict number:i];
        [self configureOrderTableCells:mainDict];
        
    }
    float completeTotal=0.0;
    float mod2Value=0.0;
    NSString *modul1Selectd=@"";
    
    for (int i=1; i<7; i++)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0];
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/Modul%d.plist",self.selectedTest ,i]];
        NSMutableDictionary *mainDict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        [self configureOrderTableHeader:mainDict number:i];
        [self configureOrderTableCellsPercentage:mainDict number:i];
        NSArray *muduleQuestions=[NSMutableArray arrayWithArray:mainDict[@"rows"]];
        NSMutableDictionary *lastDict=[NSMutableDictionary dictionaryWithDictionary:muduleQuestions.lastObject];
        if (i==1)
        {
            NSMutableDictionary *dataDict=muduleQuestions[muduleQuestions.count-2];
            if (![dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"])
            {
                modul1Selectd=dataDict[@"selected"];
            }
        }

        if (i==2)
        {
            mod2Value=[lastDict[@"total"] floatValue];
        }
        else
        {
            if (i==3)
            {
                if (mod2Value<[lastDict[@"total"] floatValue])
                {
                    completeTotal+=[lastDict[@"total"] floatValue];
                }
                else
                {
                    completeTotal+=mod2Value;
                }
            }
            else
            {
                completeTotal+=[lastDict[@"total"] floatValue];
            }  
        }
    }
    [self configureOrderTableCellsHeader2];
    NSMutableDictionary*dataDict=[NSMutableDictionary dictionary];
    [dataDict setObject:[NSString stringWithFormat:@"%.02f",completeTotal] forKey:@"total"];
    [dataDict setObject:modul1Selectd forKey:@"modul1Selectd"];
    
    [self configureOrderTableCellsTotalTable:dataDict];

}

- (void)configureOrderTableHeader:(NSMutableDictionary*)dataDict number:(int)number
{
    PDFOrderTableHeader *cell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PDFOrderTableHeader class]) owner:nil options:nil].firstObject;
    [cell setupWithModul:dataDict number:number];

    [self.stackView addSubview:cell withPrecedingMargin:10 sideMargin:0];
}

- (void)configureOrderTableCells:(NSMutableDictionary*)dataDict
{
    NSArray *muduleQuestions=[NSMutableArray arrayWithArray:dataDict[@"rows"]];

    for (NSInteger i = 0; i < muduleQuestions.count; i++) {

        PDFOrderTableCell *cell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PDFOrderTableCell class]) owner:nil options:nil].firstObject;
        [cell setupWithModul:muduleQuestions[i]];
        [self.stackView addSubview:cell withPrecedingMargin:+2 sideMargin:10];

    }
}
- (void)configureOrderTableCellsPercentage:(NSMutableDictionary*)dataDict number:(int)number
{
    NSArray *muduleQuestions=[NSMutableArray arrayWithArray:dataDict[@"rows"]];
    
        PDFOrderTableCellPercentage *cell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PDFOrderTableCellPercentage class]) owner:nil options:nil].firstObject;
        [cell setupWithModul:muduleQuestions.lastObject number:number];
        [self.stackView addSubview:cell withPrecedingMargin:+2 sideMargin:10];
}

- (void)configureOrderTableCellsTotalTable:(NSMutableDictionary*)dataDict
{
        PDFOrderTableCellTotalTable *cell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PDFOrderTableCellTotalTable class]) owner:nil options:nil].firstObject;
        [cell setupWithModul:dataDict];
        [self.stackView addSubview:cell withPrecedingMargin:2 sideMargin:10];
}
- (void)configureOrderTableCellsHeader2
{
    PDFOrderTableCellHeader2 *cell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PDFOrderTableCellHeader2 class]) owner:nil options:nil].firstObject;
    //[cell setupWithModul:muduleQuestions[i]];
    [self.stackView addSubview:cell withPrecedingMargin:2 sideMargin:10];
}


- (void)configureNoWrapViews {
    self.noWrapViews = [self.noWrapOutletCollection arrayByAddingObjectsFromArray:self.stackView.subviews];
}

@end
