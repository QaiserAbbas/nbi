#import "PDFOrderTableCellTotalTable.h"
#import <QuartzCore/QuartzCore.h>


@interface PDFOrderTableCellTotalTable ()
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UILabel *xLabel0;
@property (weak, nonatomic) IBOutlet UILabel *xlabel1;
@property (weak, nonatomic) IBOutlet UILabel *xlabel2;
@property (weak, nonatomic) IBOutlet UILabel *xlabel3;
@property (weak, nonatomic) IBOutlet UILabel *xlabel4;
@property (weak, nonatomic) IBOutlet UILabel *xlabel5;
@property (weak, nonatomic) IBOutlet UILabel *finaleStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCalculated;
@property (weak, nonatomic) IBOutlet UILabel *mod1Selected;

@end


@implementation PDFOrderTableCellTotalTable

#pragma mark - Public methods

#pragma mark - Private methods
- (void)setupWithModul:(NSDictionary*)moduleDict;
{
    float total=[moduleDict[@"total"] floatValue];
    self.totalCalculated.text=moduleDict[@"total"];
    self.mod1Selected.text=moduleDict[@"modul1Selectd"];
    
    [self.bgImageView.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.bgImageView.layer setBorderWidth: 2.0];
    
    [self unselectAllxLeabel];

    if (total>=0 && total<=12.5)
    {
        self.xLabel0.text=@"X";
        self.finaleStatusLabel.text= @"Pflegegrad 0";
    }
    else if (total>12.5 && total<=27.0)
    {
        self.xlabel1.text=@"X";

        self.finaleStatusLabel.text= @"Pflegegrad 1";
    }
    else if (total>27.0 && total<=47.5)
    {
        self.xlabel2.text=@"X";

        self.finaleStatusLabel.text= @"Pflegegrad 2";
    }
    else if (total>47.5 && total<=70.0)
    {
        self.xlabel3.text=@"X";

        self.finaleStatusLabel.text= @"Pflegegrad 3";
    }
    else if (total>70.0 && total<=90)
    {
        self.xlabel4.text=@"X";
        self.finaleStatusLabel.text= @"Pflegegrad 4";
    }
    else if (total>90)
    {
        self.xlabel5.text=@"X";
        self.finaleStatusLabel.text= @"Pflegegrad 5";
    }

}
-(void)unselectAllxLeabel
{
    self.xLabel0.text=@"";
    self.xlabel1.text=@"";
    self.xlabel2.text=@"";
    self.xlabel3.text=@"";
    self.xlabel4.text=@"";
    self.xlabel5.text=@"";
}
@end
