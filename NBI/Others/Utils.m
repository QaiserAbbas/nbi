//
//  Utils.h
//  App
//
//  Created by Qaiser Abbas on 04/08/2015.
//  Copyright (c) 2015 Bir Al Sabia. All rights reserved.


#import "Utils.h"
#import <AddressBook/AddressBook.h>
#import "SettingsClass.h"
#import "AppConstants.h"


@implementation Utils

static Utils* instance;

+(Utils*) sharedInstance
{
    if( instance == nil )
    {
        instance = [[Utils alloc] init];
    }
    
    return instance;
}

-(NSString*) getFromUserDefaults:(NSString*)key
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* objStr = [defaults objectForKey:key];
        
    return objStr;
}

-(void) setUserDefaultsValue:(NSString*)value ForKey:(NSString*)key
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

-(BOOL) isAnEmail:(NSString*)emailID
{
    if( ( [emailID rangeOfString:@"@" options:NSCaseInsensitiveSearch].location != NSNotFound ) &&
       ( [emailID rangeOfString:@".com" options:NSCaseInsensitiveSearch].location != NSNotFound ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}


-(NSString*) AppStringFromDate:(NSDate*)date
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:AppDateFormat];
    return [dateFormat stringFromDate:date];
}

-(NSString*) AppUIStringFromDate:(NSDate*)date
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:AppUIDateFormat];
    return [dateFormat stringFromDate:date];
}
-(NSString*) AppSignUpStringFromDate:(NSDate*)date
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:AppSignUpDateFormat];
    return [dateFormat stringFromDate:date];
}

-(NSDate*) AppDateFromString:(NSString*)date
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:AppDateFormat];
    return [dateFormatter dateFromString:date];
}

-(NSDate*) AppDateAfterDays:(int)days
{
    NSDateComponents* dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = days;
    NSCalendar* theCalendar = [NSCalendar currentCalendar];
    return [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
}
-(NSDate*) dateAfterDays:(int)days fromDate:(NSDate*)currentDate
{
    NSDateComponents* dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = days;
    NSCalendar* theCalendar = [NSCalendar currentCalendar];
    return [theCalendar dateByAddingComponents:dayComponent toDate:currentDate options:0];
}

-(NSString*) daysBetweenDate:(NSString*)fromDate andDate:(NSString*)toDate
{
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *startDate = [f dateFromString:fromDate];
//    NSDate *endDate = [f dateFromString:toDate];
    
    NSUInteger units = NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitYear | NSCalendarUnitMonth;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:units
                                                        fromDate:startDate
                                                          toDate:[NSDate date]
                                                         options:NSCalendarWrapComponents];
    NSString *returnStr=@"";
    
    if ([components year]>0)
    {
        if ([components year]>1)
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld years ",(long)[components year]]];
        }
        else
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld year ",(long)[components year]]];
        }
    }
    else if ([components month]>0)
    {
        if ([components month]>1)
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld months ",(long)[components month]]];
        }
        else
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld month ",(long)[components month]]];
        }
    }
    else if ([components day]>0)
    {
        if ([components day]>1)
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld days ",(long)[components day]]];
        }
        else
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld day ",(long)[components day]]];
        }
    }
    
    else if ([components hour]>0)
    {
        if ([components hour]>1)
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld hours ",(long)[components hour]]];
        }
        else
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld hour ",(long)[components hour]]];
 
        }
    }
    else if ([components minute]>0)
    {
        if ([components minute]>1)
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld minutes ",(long)[components minute]]];
        }
        else
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld minute ",(long)[components minute]]];
        }

    }
    else if ([components second]>0)
    {
        if ([components second]>1)
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld seconds ",(long)[components second]]];
        }
        else
        {
            returnStr=[returnStr stringByAppendingString:[NSString stringWithFormat:@"%ld second ",(long)[components second]]];
        }
    }
    

    return returnStr;
    
}

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}

- (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}
- (UIColor *)darkerColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MAX(r - 0.2, 0.0)
                               green:MAX(g - 0.2, 0.0)
                                blue:MAX(b - 0.2, 0.0)
                               alpha:a];
    return nil;
}




@end
