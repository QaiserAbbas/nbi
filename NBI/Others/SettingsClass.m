//
//  SettingsClass.m
//  MyRentalLog
//
//  Created by JunaidAkram on 2/3/14.
//  Copyright (c) 2014 swenggco-software. All rights reserved.
//

#import "SettingsClass.h"
#import <CoreMotion/CMMotionActivityManager.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import "UserObject.h"
#import "SingletonClass.h"

CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};
CGFloat RadiansToDegrees(CGFloat radians) {return radians * 180/M_PI;};


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define IS_IPHONE_4 ( [ [ UIScreen mainScreen ] bounds ].size.height == 480 )


#define IS_IPAD ( [ [ UIScreen mainScreen ] bounds ].size.width == 1024 )

#define IS_IPHONE_6      (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)

@implementation SettingsClass

#pragma  mark --
#pragma  mark -- Devices

+(BOOL)ios7;
{
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    
    if (sysVer >=7)
    {
        return YES;
    }
    return NO;
}
+(BOOL)ios8;
{
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    
    if (sysVer >=8)
    {
        return YES;
    }
    return NO;
}
+(BOOL)isiPhone4;
{
    BOOL isiPhone4=FALSE;
    
    if(IS_IPHONE_4)
    {
        isiPhone4=TRUE;
    }
    
    return isiPhone4;
}
+(BOOL)isiPhone5;
{
    BOOL isiPhone5=FALSE;
    
    if( IS_IPHONE_5 )
    {
        isiPhone5=TRUE;
    }

    return isiPhone5;
}
+(BOOL)isiPhone6;
{
    BOOL isiPhone6=FALSE;
    
    if( IS_IPHONE_6 )
    {
        isiPhone6=TRUE;
    }
    
    return isiPhone6;
}
+(BOOL)isiPhone6Plus;
{
    BOOL isiPhone6Plus=FALSE;
    
    if( IS_IPHONE_6_PLUS )
    {
        isiPhone6Plus=TRUE;
    }
    
    return isiPhone6Plus;
}
+(BOOL)isiPhone;
{
    BOOL isIphone=FALSE;
    
    if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone))
    {
        isIphone=TRUE;
    }
    
    return isIphone;
}
+(BOOL)isiPAD;
{
    BOOL isIphone=FALSE;
    
    if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))
    {
        isIphone=TRUE;
    }
    
    return isIphone;
}


+(void)setCenterWithRexpectToSuperview:(float)superviewWidth subviewWidth:(float)subviewWidth sibview:(UIView*)view;
{
    CGRect frame=view.frame;
    frame.origin.x=(superviewWidth-subviewWidth)/2;
    view.frame=frame;
}

+(void)setXAxis:(UIView*)view float:(float)value;
{
    CGRect frame=view.frame;
    frame.origin.x-=value;
    view.frame=frame;
}

+(void)setYAxis:(UIView*)view float:(float)value;
{
    CGRect frame=view.frame;
    frame.origin.y-=value;
    view.frame=frame;
}

+(void)setXAxistozero:(UIView*)view;
{
    CGRect frame=view.frame;
    frame.origin.x=0;
    view.frame=frame;
}

+(void)setxAxisIfXvalue:(float)value float:(float)requiredvalue view:(UIView*)view;
{
    CGRect frame=view.frame;

    
    if (frame.origin.x==value)
    {
        frame.origin.x-=requiredvalue;
    }
    
    view.frame=frame;
}


+(void)setWidthIfXvalue:(float)value float:(float)requiredWidth view:(UIView*)view;
{
    CGRect frame=view.frame;
    if (frame.origin.x==value)
    {
        frame.size.width+=requiredWidth;
    }
    
    view.frame=frame;
}

+(void)setWidth:(float)width view:(UIView*)view;
{
    CGRect frame=view.frame;
    frame.size.width=width;
    view.frame=frame;
}

+(void)setHeight:(float)height view:(UIView*)view;
{
    CGRect frame=view.frame;
    frame.size.height=height;
    view.frame=frame;
}
+(void)setchangeInHeight:(UIView*)view float:(float)value;
{
    CGRect frame=view.frame;
    frame.size.height-=value;
    view.frame=frame;
    
}
+(void)setXAxisToPoint:(UIView*)view float:(float)value;
{
    CGRect frame=view.frame;
    frame.origin.x=value;
    view.frame=frame;

}


#pragma --
#pragma -- Image Manipulation
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
    CGContextRef abc=  UIGraphicsGetCurrentContext();
    CGContextRelease(abc);

    return newImage;
}
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToHeigh: (float) i_height
{
    float oldHeight = sourceImage.size.height;
    float scaleFactor = i_height / oldHeight;
    
    float newWidth = sourceImage.size.width * scaleFactor;
    float newHeight = oldHeight * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
    CGContextRef abc=  UIGraphicsGetCurrentContext();
    CGContextRelease(abc);

    return newImage;
}


#pragma mark --
#pragma mark Email Validation
+(BOOL)isValidEmail:(NSString*)email;
{
    BOOL returnBool=NO;
    
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    //Valid email address
    if ([emailTest evaluateWithObject:email] == YES)
    {
        returnBool=YES;
    }
    else
    {
        returnBool=NO;
        
    }
    return returnBool;
    
}


#pragma mark --
#pragma mark iCloud Data storage chnage

+(void)removeFileFromBackup:(NSString*)directory;
{
    NSURL *urlFromPath = [[NSURL alloc] initFileURLWithPath:directory];
    
    if ([self addSkipBackupAttributeToItemAtURL:urlFromPath])
    {
    }
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
{
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

+(UIImage *)imagefromVideo :(NSURL*)url
{
    AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
    generate1.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 2);
    CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
    return [[UIImage alloc] initWithCGImage:oneRef];
}
+ (UIImage *)imageWithImageHardCoded:(UIImage *)image size:(CGSize)sizetoscale;
{
//    CGSize sizetoscale=CGSizeMake(93, 75);
    
    CGFloat scale = MAX(sizetoscale.width/image.size.width, sizetoscale.height/image.size.height);
    CGFloat width = image.size.width * scale;
    CGFloat height = image.size.height * scale;
    CGRect imageRect = CGRectMake((sizetoscale.width - width)/2.0f,
                                  (sizetoscale.height - height)/2.0f,
                                  width,
                                  height);
    
    UIGraphicsBeginImageContextWithOptions(sizetoscale, NO, 0);
    [image drawInRect:imageRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageWithSquareImage:(UIImage *)image;
{
    CGSize sizetoscale=CGSizeMake(400, 400);
    if (image.size.height>image.size.width)
    {
        sizetoscale=CGSizeMake(image.size.width, image.size.width);
    }
    else
    {
        sizetoscale=CGSizeMake(image.size.height, image.size.height);
    }
    CGFloat scale = MAX(sizetoscale.width/image.size.width, sizetoscale.height/image.size.height);
    CGFloat width = image.size.width * scale;
    CGFloat height = image.size.height * scale;
    CGRect imageRect = CGRectMake((sizetoscale.width - width)/2.0f,
                                  (sizetoscale.height - height)/2.0f,
                                  width,
                                  height);
    
    UIGraphicsBeginImageContextWithOptions(sizetoscale, NO, 0);
    [image drawInRect:imageRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage*)previewFromFileAtPath:(NSString*)path ratio:(CGFloat)ratio
{
    AVAsset *asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:path]];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime duration = asset.duration;
    CGFloat durationInSeconds = duration.value / duration.timescale;
    CMTime time = CMTimeMakeWithSeconds(durationInSeconds * ratio, (int)duration.value);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    
    return thumbnail;
}


+ (UIImage*)compressImage:(UIImage*)OriginaleImage;
{
    float actualHeight = OriginaleImage.size.height;
    float actualWidth = OriginaleImage.size.width;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = 640.0/960.0;
    
    if(imgRatio!=maxRatio){
        if(imgRatio < maxRatio){
            imgRatio = 960.0 / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = 960.0;
        }
        else{
            imgRatio = 640.0 / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = 640.0;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [OriginaleImage drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

+ (UIImage*) rotateImageAppropriately:(UIImage*)imageToRotate
{
    UIImage* properlyRotatedImage;
    
    CGImageRef imageRef = [imageToRotate CGImage];
    
    if (imageToRotate.imageOrientation == 0)
    {
        properlyRotatedImage = imageToRotate;
    }
    else if (imageToRotate.imageOrientation == 3)
    {
        
        CGSize imgsize = imageToRotate.size;
        UIGraphicsBeginImageContext(imgsize);
        [imageToRotate drawInRect:CGRectMake(0.0, 0.0, imgsize.width, imgsize.height)];
        properlyRotatedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else if (imageToRotate.imageOrientation == 1)
    {
        properlyRotatedImage = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:1];
    }
    
    return properlyRotatedImage;
}


+ (UIImage *)imageRotatedByDegrees:(CGFloat)degrees imageRotation:(UIImage*)imageToRotate
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,imageToRotate.size.width, imageToRotate.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-imageToRotate.size.width / 2, -imageToRotate.size.height / 2, imageToRotate.size.width, imageToRotate.size.height), [imageToRotate CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}


+(void)saveUser:(NSMutableDictionary*)dataDict rememberMe:(BOOL)rememberMe
{
    UserObject *user=[[UserObject alloc] init];
    
    NSLog(@"%@",dataDict[@"mExtra"]);
    user.user_id = [NSString stringWithFormat:@"%@",dataDict[@"mExtra"][App_User_ID]];
    user.email = [NSString stringWithFormat:@"%@",dataDict[@"mExtra"][App_User_Email]];
    user.user_type = [NSString stringWithFormat:@"%@",dataDict[@"mExtra"][App_User_UserType]];
    
    user.phone_number = [NSString stringWithFormat:@"%@",dataDict[@"mExtra"][App_User_PhoneNumber]];
    user.first_name = [NSString stringWithFormat:@"%@",dataDict[@"mExtra"][App_User_FirstName]];
    user.last_name = [NSString stringWithFormat:@"%@",dataDict[@"mExtra"][App_User_LastName]];
    user.profile_pic_path = [NSString stringWithFormat:@"%@",dataDict[@"mExtra"][App_User_Profile_Pic_Path]];
    
    
    [[SingletonClass sharedSingletonClass].settingsDictionary setObject:user forKey:@"User"];
    if (rememberMe)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:@"User"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+(NSString*)decodeBase64String:(NSString*)string
{
    if (![string isKindOfClass:[NSNull class]])
    {
        if ([string length] > 1)
        {
            if ([string rangeOfString:@"\n"].location == NSNotFound)
            {
            }
            else
            {
                string = [string substringToIndex:[string length] - 2];
            }
        }
        else
        {
            //no characters to delete... attempting to do so will result in a crash
        }
        
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:string options:0];
        NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        if ([decodedString isEqualToString:@""] || !decodedString)
        {
            return string;
        }
        return decodedString;
    }
    else
    {
        return @"";
    }
}
+(NSString*)encodeStringTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    } else {
        base64String = [plainData base64Encoding];                              // pre iOS7
    }
    
    return base64String;
}


+(NSString *)auth_key
{
    NSString*authKey=[[NSUserDefaults standardUserDefaults] stringForKey:@"auth_key"];
    if (!authKey)
    {
        authKey=@"060fac9a80afec9b95eb292ad884c5f5";
    }
    return authKey;
}

+(void)setAuth_key:(NSString *)auth_key
{
    if (auth_key)
    {
        [[NSUserDefaults standardUserDefaults] setValue:auth_key forKey:@"auth_key"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
+(void)RemoveAuth_key
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"auth_key"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
