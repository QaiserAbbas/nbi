//
//  Utils.h
//  App
//
//  Created by Qaiser Abbas on 04/08/2015.
//  Copyright (c) 2015 Bir Al Sabia. All rights reserved.

#import <Foundation/Foundation.h>
#import "AppDelegate.h"


@interface Utils : NSObject
{
    AppDelegate *delegate;
}

+(Utils*) sharedInstance;

-(NSString*) getFromUserDefaults:(NSString*)key;
-(void) setUserDefaultsValue:(NSString*)value ForKey:(NSString*)key;

-(BOOL) isAnEmail:(NSString*)emailID;


-(NSString*) AppStringFromDate:(NSDate*)date;
-(NSString*) AppUIStringFromDate:(NSDate*)date;
-(NSDate*) AppDateFromString:(NSString*)date;
-(NSString*) AppSignUpStringFromDate:(NSDate*)date;
-(NSDate*) AppDateAfterDays:(int)days;
-(NSDate*) dateAfterDays:(int)days fromDate:(NSDate*)currentDate;
-(NSString*) daysBetweenDate:(NSString*)fromDate andDate:(NSString*)toDate;

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
- (unsigned int)intFromHexString:(NSString *)hexStr;
- (UIColor *)darkerColorForColor:(UIColor *)c;



@end
