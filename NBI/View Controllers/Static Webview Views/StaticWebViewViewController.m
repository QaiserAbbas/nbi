//
//  StaticWebViewViewController.m
//  BeRain
//
//  Created by Qaiser Abbas on 12/24/17.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

#import "StaticWebViewViewController.h"

@interface StaticWebViewViewController ()<UIWebViewDelegate>

@end

@implementation StaticWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *urlAddress = @"";
    [viewTitleLabel setText:NSLocalizedString(@"NBI", @"")];
    
    if ([self.viewName isEqualToString:@"producte"])
    {
        urlAddress=[NSString stringWithFormat:@"http://www.komda.de/"];
    }
    
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [staticWebView loadRequest:requestObj];
    staticWebView.delegate=self;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (webView.isLoading)
        return;
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
