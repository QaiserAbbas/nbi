//
//  BaseTableViewController.h
//  NBI
//
//  Created by Qaiser Abbas on 2/27/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UITableViewController
{
    IBOutlet UITableView *baseTableView;
    
}
@end
