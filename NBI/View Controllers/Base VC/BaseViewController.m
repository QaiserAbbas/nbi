//
//  BaseViewController.m
//  IWasHere
//
//  Created by Qaiser Abbas on 21/10/2015.
//  Copyright © 2015 BirAlSabia. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}


- (IBAction)menuButtonTap:(UIButton *)sender
{
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
}

@end
