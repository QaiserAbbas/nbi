//
//  BaseViewController.h
//  IWasHere
//
//  Created by Qaiser Abbas on 21/10/2015.
//  Copyright © 2015 BirAlSabia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsClass.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "AppConstants.h"
#import "SingletonClass.h"
#import "UserObject.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "LGAlertView.h"
#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "DataManager.h"

@interface BaseViewController : UIViewController
{
    
}


- (IBAction)menuButtonTap:(UIButton *)sender;



@end
