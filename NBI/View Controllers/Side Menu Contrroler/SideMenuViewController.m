//
//  SideMenuViewController.m
//  TrueLie
//
//  Created by Qaiser Abbas on 26/11/2015.
//  Copyright © 2015 Bir Al Sabia. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "StaticWebViewViewController.h"
#import "iRate.h"
#import "ImressumViewController.h"

@interface SideMenuViewController ()
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.slideOutAnimationEnabled = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{

}
-(void)updateProfile
{
}

- (IBAction)NBIButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:[SlideNavigationController sharedInstance].viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[HomeViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        HomeViewController* homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:homeViewController animated:YES];
        
    }
}

- (IBAction)producteButtonTap:(UIButton *)sender
{
    [self manageStaticWebView:@"producte"];
}

- (IBAction)bewertenButtonTap:(UIButton *)sender
{
    [[iRate sharedInstance] promptForRating];
}

- (IBAction)imressionButtonTap:(UIButton *)sender
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:[SlideNavigationController sharedInstance].viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[ImressumViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        ImressumViewController* imressumViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ImressumViewController"];
        [[SlideNavigationController sharedInstance] pushViewController:imressumViewController animated:YES];
        
    }

}


-(void)manageStaticWebView:(NSString*)viewName
{
    bool alreadyPushed = false;
    //Check if the view was already pushed
    NSMutableArray *viewControllers;
    if ( (viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers])) {
        
        for (UIViewController *aViewController in viewControllers) {
            
            if ([aViewController isKindOfClass:[StaticWebViewViewController class]]) {
                [[SlideNavigationController sharedInstance] popToViewController:aViewController animated:YES];
                StaticWebViewViewController *staticWebViewViewController=(StaticWebViewViewController*)aViewController;
                staticWebViewViewController.viewName=viewName;
                alreadyPushed = true;
                break;
            }
        }
    }
    
    //Push Fresh View
    if( alreadyPushed == false)
    {
        StaticWebViewViewController* staticWebViewViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"StaticWebViewViewController"];
        staticWebViewViewController.viewName=viewName;
        [[SlideNavigationController sharedInstance] pushViewController:staticWebViewViewController animated:YES];
    }

}

@end
