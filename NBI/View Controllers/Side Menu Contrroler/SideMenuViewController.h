//
//  SideMenuViewController.h
//  TrueLie
//
//  Created by Qaiser Abbas on 26/11/2015.
//  Copyright © 2015 Bir Al Sabia. All rights reserved.
//

#import "BaseViewController.h"
#import "SingletonClass.h"


@interface SideMenuViewController : BaseViewController
{
}
-(void)updateProfile;
- (IBAction)NBIButtonTap:(UIButton *)sender;
- (IBAction)producteButtonTap:(UIButton *)sender;
- (IBAction)bewertenButtonTap:(UIButton *)sender;
- (IBAction)imressionButtonTap:(UIButton *)sender;

@end
