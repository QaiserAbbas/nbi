//
//  SavedTestTableViewCell.h
//  NBI
//
//  Created by Qaiser Abbas on 3/5/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedTestTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dobLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *indicatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *pgText;
@property (weak, nonatomic) IBOutlet UIImageView *bottomImageView;

@end
