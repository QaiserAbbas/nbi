//
//  HomeViewController.h
//  NBI
//
//  Created by Qaiser Abbas on 2/15/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import "BaseViewController.h"
#import <EHHorizontalSelectionView/EHHorizontalSelectionView.h>
#import "PDFOrderView.h"
#import <MAGPdfGenerator/MAGPdfGenerator.h>

//#import <MAGPdfGenerator/MAGPdfGenerator-umbrella.h>

@interface HomeViewController : BaseViewController<UITableViewDataSource,
UITableViewDelegate,LGAlertViewDelegate,UITextFieldDelegate,UIDocumentInteractionControllerDelegate, MAGPdfRendererDelegate>
{
    int selectedTest;
    NSMutableArray *savedTestArray;
    __weak IBOutlet UITableView *saveTestTableView;
    BOOL update;
    NSMutableDictionary*resultDict;
    
    NSMutableArray *muduleArray;
    NSMutableDictionary *mainDict;
    int moduleSelected;
    
    __weak IBOutlet UILabel *titleLabel;
    
    IBOutlet EHHorizontalSelectionView *horizentolScrollView;
    __weak IBOutlet UILabel *scoreLabel;
    __weak IBOutlet UIView *interoView;
    __weak IBOutlet UIView *moduleView;

    NSMutableArray *cell0SubMenuItemsArray;
    
    BOOL isSection0Cell0Expanded;
    IBOutlet UITableView *tblView;
    NSString *headerTitle;
    NSMutableArray *muduleQuestions;
    NSMutableArray *mcqArray;
    NSString *mcqTitle;
    NSMutableDictionary* selectedDict;
    
    __weak IBOutlet UIImageView *backGroundImageView;
    __weak IBOutlet UIView *module5PopUp;
    __weak IBOutlet UILabel *module5PopUpTitle;
    __weak IBOutlet UITextField *proTagTextfiled;
    __weak IBOutlet UITextField *proWocheTextfiled;
    __weak IBOutlet UITextField *proManatTextfiled;
    __weak IBOutlet UIImageView *proTagImageView;
    __weak IBOutlet UIImageView *proWocheImageView;
    __weak IBOutlet UIImageView *proManatImageView;
    
    __weak IBOutlet UIView *resultView;
    __weak IBOutlet UILabel *finalScoreM1;
    __weak IBOutlet UILabel *finalScoreM2;
    __weak IBOutlet UILabel *finalScoreM4;
    __weak IBOutlet UILabel *finalScoreM5;
    __weak IBOutlet UILabel *finalScoreM6;
    __weak IBOutlet UILabel *Besonderer;
    __weak IBOutlet UILabel *completeTotal;
    __weak IBOutlet UILabel *pdfLabel;
    __weak IBOutlet UIImageView *finamM1ImageView;
    __weak IBOutlet UIImageView *finamM12mageView;
    __weak IBOutlet UIImageView *finamM14mageView;
    __weak IBOutlet UIImageView *finamM15mageView;
    __weak IBOutlet UIImageView *finamM16mageView;
    
    BOOL allComplete;
    
    __weak IBOutlet UIView *saveName;
    __weak IBOutlet UITextField *nameTextField;
    __weak IBOutlet UITextField *dobTextField;
    __weak IBOutlet UIImageView *saveNameImageView;
    __weak IBOutlet UIImageView *nameImageView;
    __weak IBOutlet UIImageView *dobImageview;
    
    __weak IBOutlet UIView *clearAllView;
    __weak IBOutlet UIImageView *clearAllImageView;
    __weak IBOutlet UIButton *deleteButton;
}
@property (nonatomic) PDFOrderView *pdfOrderView;


- (IBAction)eraseData:(UIButton *)sender;
- (IBAction)nextButtonTap:(UIButton *)sender;
- (IBAction)entfalltButtonTap:(UIButton *)sender;
- (IBAction)werteButtonTap:(UIButton *)sender;
- (IBAction)selbstandigButtonTap:(UIButton *)sender;

- (IBAction)m1ButtonTap:(UIButton *)sender;
- (IBAction)m2ButtonTap:(UIButton *)sender;
- (IBAction)m4ButtonTap:(UIButton *)sender;
- (IBAction)m5ButtonTap:(UIButton *)sender;
- (IBAction)m6ButtonTap:(UIButton *)sender;
- (IBAction)pdfButtonTap:(UIButton *)sender;
- (IBAction)refershAllPlistsButtonTap:(UIButton *)sender;
- (IBAction)löschenButtonTap:(UIButton *)sender;
- (IBAction)saveButtonTap:(UIButton *)sender;

- (IBAction)saveNameButtonTap:(UIButton *)sender;

@end
