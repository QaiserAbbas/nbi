//
//  HomeViewController.m
//  NBI
//
//  Created by Qaiser Abbas on 2/15/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import "HomeViewController.h"
#import "CustomHorizontalViewCell.h"
#import "DetailTableViewCell.h"
#import "DetailHeaderTableViewCell.h"
#import "MCQOptionTableViewCell.h"
#import "SavedTestTableViewCell.h"

@interface HomeViewController ()<EHHorizontalSelectionViewProtocol, UIDocumentInteractionControllerDelegate>

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    muduleArray=[NSMutableArray array];
    [muduleArray addObject:@"STARTSEITE"];
    [muduleArray addObject:@"MODUL 1"];
    [muduleArray addObject:@"MODUL 2"];
    [muduleArray addObject:@"MODUL 3"];
    [muduleArray addObject:@"MODUL 4"];
    [muduleArray addObject:@"MODUL 5"];
    [muduleArray addObject:@"MODUL 6"];
    [muduleArray addObject:@"ERGEBNIS"];
    [interoView setHidden:NO];
    [moduleView setHidden:YES];
    
    horizentolScrollView.delegate = self;
    [horizentolScrollView registerCellNib:[UINib nibWithNibName:@"CustomHorizontalViewCell" bundle:nil] withClass:[CustomHorizontalViewCell class]];
    //Set font for _hSelView3 selection view
    [horizentolScrollView setFont:[UIFont systemFontOfSize:12]];
    [horizentolScrollView setTextColor:[UIColor blackColor]];
    [horizentolScrollView setCellGap:70.f];
    [horizentolScrollView setTintColor:[UIColor blackColor]];
    [horizentolScrollView setClipsToBounds:YES];
    moduleSelected=1;
    selectedTest=1;
    update=YES;
    resultDict=[NSMutableDictionary dictionary];
    
    [self getListOfSavedTests];
    [self loadTableViewFromPList];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(hideView)];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(hideView)];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                           action:@selector(hideView)];

    [backGroundImageView addGestureRecognizer:tap3];
    [clearAllImageView addGestureRecognizer:tap2];
    [saveNameImageView addGestureRecognizer:tap];
    
    allComplete=YES;
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
}
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
}
-(void)getListOfSavedTests
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"SavedTest.plist"]];
    
    // Build the array from the plist
    savedTestArray=[NSMutableArray arrayWithContentsOfFile:plistPath];
    
    NSArray *oldArray=[NSArray arrayWithArray:savedTestArray];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:NO];
    savedTestArray=[NSMutableArray arrayWithArray:[oldArray sortedArrayUsingDescriptors:@[sort]]];

    if (savedTestArray.count>0)
    {
        selectedTest=(int)savedTestArray.count;
    }
    else
    {
        [self saveDummyTest];
        selectedTest=1;
        update=NO;
    }
    
}

-(void)loadTableViewFromPList
{
    NSMutableDictionary*delDict=[NSMutableDictionary dictionary];
    
    if (savedTestArray.count>selectedTest-1)
    {
        delDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];
    }
    else
    {
        if (savedTestArray.count>0)
        {
            delDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray.lastObject];
            int foldername=[delDict[@"folderName"] intValue]+1;
            [delDict setObject:[NSString stringWithFormat:@"%d",foldername] forKey:@"folderName"];
        }
        else
        {
            [delDict setObject:@"1" forKey:@"folderName"];
        }
    }
    if ([delDict[@"name"] length]>0)
    {
        [titleLabel setText:[NSString stringWithFormat:@"NBI - %@",delDict[@"name"]]];
    }
    else
    {
        [titleLabel setText:@"NBI - Neue Begutachtung"];
    }
    
    cell0SubMenuItemsArray = [NSMutableArray array];
    muduleQuestions=[NSMutableArray array];
    mcqArray=[NSMutableArray array];
    selectedDict=[NSMutableDictionary dictionary];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@/Modul%d.plist",delDict[@"folderName"],moduleSelected]];
    
    // Build the array from the plist
    mainDict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    if (mainDict)
    {
        [cell0SubMenuItemsArray addObject:mainDict[@"description"]];
        muduleQuestions=[NSMutableArray arrayWithArray:mainDict[@"rows"]];
        headerTitle=mainDict[@"title"];
        [scoreLabel setText:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:NO]];
    }
    [tblView setContentOffset:CGPointZero animated:YES];

    [tblView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - EHHorizontalSelectionViewProtocol

- (NSUInteger)numberOfItemsInHorizontalSelection:(EHHorizontalSelectionView*)hSelView
{
    return [muduleArray count];
}

- (NSString *)titleForItemAtIndex:(NSUInteger)index forHorisontalSelection:(EHHorizontalSelectionView*)hSelView
{
    return muduleArray[index];
}

- (EHHorizontalViewCell *)selectionView:(EHHorizontalSelectionView *)selectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CustomHorizontalViewCell * cell = (CustomHorizontalViewCell*)[selectionView dequeueReusableCellWithReuseIdentifier:[CustomHorizontalViewCell reuseIdentifier] forIndexPath:indexPath];
    cell.titleLabel.text = muduleArray[indexPath.row];;
    //    cell.iconView.hidden= YES;
    
    return cell;
}
- (void)horizontalSelection:(EHHorizontalSelectionView *)selectionView didSelectObjectAtIndex:(NSUInteger)index
{
    moduleSelected=(int)index;
    
    if (index==0)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0];
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"SavedTest.plist"]];
        
        // Build the array from the plist
        savedTestArray=[NSMutableArray arrayWithContentsOfFile:plistPath];
        NSArray *oldArray=[NSArray arrayWithArray:savedTestArray];
        
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:NO];
        savedTestArray=[NSMutableArray arrayWithArray:[oldArray sortedArrayUsingDescriptors:@[sort]]];

        [saveTestTableView reloadData];
        
        [interoView setHidden:NO];
        [moduleView setHidden:YES];
        [resultView setHidden:YES];
    }
    else if (index==7)
    {
        [interoView setHidden:YES];
        [moduleView setHidden:YES];
        [resultView setHidden:NO];
        [self calculateFinaleScore];
    }
    else
    {
        [interoView setHidden:YES];
        [moduleView setHidden:NO];
        [resultView setHidden:YES];
        [self loadTableViewFromPList];
    }
    [tblView reloadData];
    
    [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


# pragma mark - UITableView Delegate and Datasource
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (tableView==saveTestTableView)
    {
        return 50;
    }
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView==saveTestTableView)
    {
        NSMutableArray*completeArray=[NSMutableArray array];
        for (NSMutableDictionary*testDict in savedTestArray)
        {
            if ([testDict[@"name"] length]>0)
            {
                [completeArray addObject:testDict];
            }
        }
        return savedTestArray.count;
    }

    if (section == 0)
    {
        int cellCount = (int)muduleQuestions.count; // Default count - if not a single cell is expanded
        
        if (isSection0Cell0Expanded)
        {
            cellCount += [cell0SubMenuItemsArray count];
        }
        
        return cellCount;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==saveTestTableView)
    {
        static NSString* Identifier = @"SavedTestTableViewCell";
        
        SavedTestTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
        if( cell == nil )
        {
            NSString* cellName = @"SavedTestTableViewCell";
            if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                //                cellName = @"LiveEventsTableViewCell-iPad";
            }
            NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSMutableDictionary *dataDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[indexPath.row]];
        if ([dataDict[@"name"] length]>0)
        {
            [cell.nameLabel setHidden:NO];
            [cell.indicatorImageView setHidden:NO];
            [cell.dobLabel setHidden:NO];
            [cell.levelLabel setHidden:NO];
            [cell.pgText setHidden:NO];
            [cell.bottomImageView setHidden:NO];
            

            cell.nameLabel.text=[NSString stringWithFormat:@"%@ (*%@)",dataDict[@"name"],dataDict[@"dob"]];
            NSString *dateStr=dataDict[@"date"];
            
            if ([dateStr length] > 0) {
                dateStr = [dateStr substringToIndex:[dateStr length] - 5];
            } else {
                //no characters to delete... attempting to do so will result in a crash
            }
            
            cell.dobLabel.text=[NSString stringWithFormat:@"begutachtet am %@",dateStr];
            cell.levelLabel.text=dataDict[@"status"];
            if ([dataDict[@"complete"] boolValue])
            {
                [cell.indicatorImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor greenColor]]];
            }
            else
            {
                [cell.indicatorImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor redColor]]];
            }
        }
        else
        {
            [cell.nameLabel setHidden:YES];
            [cell.indicatorImageView setHidden:YES];
            [cell.dobLabel setHidden:YES];
            [cell.levelLabel setHidden:YES];
            [cell.pgText setHidden:YES];
            [cell.bottomImageView setHidden:YES];
        }
        
        return cell;
        
    }
    else
    {
        if (indexPath.section == 0)
        {
        if (indexPath.row == 0)
        {
            static NSString* Identifier = @"DetailHeaderTableViewCell";
            
            DetailHeaderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
            if( cell == nil )
            {
                NSString* cellName = @"DetailHeaderTableViewCell";
                if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    //                cellName = @"LiveEventsTableViewCell-iPad";
                }
                NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            if (isSection0Cell0Expanded) // Set accessory view according to cell state - EXPANDED / NOT EXPANDED
            {
                cell.expandImageView.image = [UIImage imageNamed:@"col_arrow"];
            }
            else
            {
                cell.expandImageView.image = [UIImage imageNamed:@"exp_arrow"];
            }
            cell.headerTitle.text=headerTitle;
            return cell;
        }
        else
        {
            if (isSection0Cell0Expanded && [cell0SubMenuItemsArray count] >= indexPath.row) // Check Expanded status and do the necessary changes
            {
                static NSString* Identifier = @"DetailTableViewCell";
                
                DetailTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
                if( cell == nil )
                {
                    NSString* cellName = @"DetailTableViewCell";
                    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        //                cellName = @"LiveEventsTableViewCell-iPad";
                    }
                    NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }

                cell.detailLabel.text = [NSString stringWithFormat:@"%@", [cell0SubMenuItemsArray objectAtIndex:indexPath.row - 1]];
                return cell;
            }
            else
            {
                static NSString* Identifier = @"MCQOptionTableViewCell";
                
                MCQOptionTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
                if( cell == nil )
                {
                    NSString* cellName = @"MCQOptionTableViewCell";
                    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        //                cellName = @"LiveEventsTableViewCell-iPad";
                    }
                    NSArray* nib = [[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
                    cell = [nib objectAtIndex:0];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                NSMutableDictionary *dataDict=[NSMutableDictionary dictionary];
                if (isSection0Cell0Expanded)
                {
                    dataDict=muduleQuestions[indexPath.row-2];
                    cell.numberLabel.text=[NSString stringWithFormat:@"%d.%ld",moduleSelected,indexPath.row-1];
                    
                }
                else
                {
                    dataDict=muduleQuestions[indexPath.row-1];
                    cell.numberLabel.text=[NSString stringWithFormat:@"%d.%ld",moduleSelected,indexPath.row];
                }
                cell.questionLabel.text=dataDict[@"title"];
                cell.answeredLabel.text=dataDict[@"selected"];
                if (![dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"])
                {
                    [cell.redImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor greenColor]]];
                }
                else
                {
                    [cell.redImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor redColor]]];
                }

                return cell;
            }
        }
    }
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==saveTestTableView)
    {
        NSMutableDictionary *dataDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[indexPath.row]];
        if ([dataDict[@"name"] length]>0)
        {
            selectedTest=(int)indexPath.row+1;
            [self loadTableViewFromPList];
            [horizentolScrollView selectIndex:1];
            moduleSelected=1;
        }
        else
        {
            
        }
    }
    else
    {
    if (indexPath.row == 0)
    {
        // Change status of a cell reload table
        
        isSection0Cell0Expanded = !isSection0Cell0Expanded;
        
        if (isSection0Cell0Expanded)
        {
            NSArray *cells = [NSArray arrayWithObjects:
                              [NSIndexPath indexPathForRow:1 inSection:0],
                              nil];
            
            [CATransaction begin];
            
            [CATransaction setCompletionBlock:^{
                [tblView reloadData];
            }];
            
            [tblView beginUpdates];
            [tblView insertRowsAtIndexPaths:cells withRowAnimation:UITableViewRowAnimationFade];
            [tblView endUpdates];
            
            [CATransaction commit];
            
        }
        else
        {
            NSArray *cells = [NSArray arrayWithObjects:
                              [NSIndexPath indexPathForRow:1 inSection:0],
                              nil];
            
            [CATransaction begin];
            
            [CATransaction setCompletionBlock:^{
                [tblView reloadData];
            }];
            
            [tblView beginUpdates];
            [tblView deleteRowsAtIndexPaths:cells withRowAnimation:UITableViewRowAnimationFade];
            [tblView endUpdates];
            
            [CATransaction commit];
        }
    }
    else
    {
        if (isSection0Cell0Expanded)
        {
            if (indexPath.row>1)
            {
                [self showTheMCQPopUp:(int)indexPath.row];
            }
        }
        else
        {
            [self showTheMCQPopUp:(int)indexPath.row];
        }
    }
    }
}
-(void)showTheMCQPopUp:(int)tag
{
    int row=0;
    
    if (isSection0Cell0Expanded)
    {
        row=(int)tag-2;
    }
    else
    {
        row=(int)tag-1;
    }
    selectedDict=muduleQuestions[row];

    mcqArray=[NSMutableArray arrayWithArray:selectedDict[@"mcqArray"]];
    mcqTitle=selectedDict[@"titleMcq"];
    
    if (moduleSelected==5 && [selectedDict[@"type"] intValue] !=0)
    {
        [proWocheTextfiled setText:@""];
        [proManatTextfiled setText:@""];
        [proTagTextfiled setText:@""];
        
        [module5PopUpTitle setText:mcqTitle];
        [module5PopUp setHidden:NO];
        [self.view bringSubviewToFront:module5PopUp];
        
        module5PopUp.tag=tag;
        
        if ([selectedDict[@"type"] intValue] ==3)
        {
            [proTagTextfiled setHidden:YES];
            [proTagImageView setHidden:YES];
            [proWocheTextfiled becomeFirstResponder];
        }
        else
        {
            [proTagTextfiled setHidden:NO];
            [proTagImageView setHidden:NO];
            [proTagTextfiled becomeFirstResponder];
        }
        return;
    }
    NSMutableArray *titleArray=[NSMutableArray array];
    
    for (NSDictionary*dataDict in mcqArray)
    {
        [titleArray addObject:dataDict[@"title"]];
    }
  //  if ([selectedDict[@"type"] intValue]==0)
    {
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:mcqTitle
                                                            message:@""
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:titleArray
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                           delegate:self];
        alertView.buttonsTextAlignment = NSTextAlignmentLeft;
        
        alertView.cancelButtonTextAlignment = NSTextAlignmentCenter;
        alertView.tag=tag;
        [alertView showAnimated:YES completionHandler:nil];
    }

}

-(void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
    MCQOptionTableViewCell *cell = (MCQOptionTableViewCell *)[tblView cellForRowAtIndexPath:indexPath];
    cell.answeredLabel.text=title;
    [cell.redImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor greenColor]]];

    int row=0;
    
    if (isSection0Cell0Expanded)
    {
        row=(int)indexPath.row-2;
    }
    else
    {
        row=(int)indexPath.row-1;
    }

    NSDictionary*titleDict=[NSMutableDictionary dictionaryWithDictionary:mcqArray[index]];
    
    [selectedDict setObject:[NSString stringWithFormat:@"%d",[titleDict[@"value"] intValue]] forKey:@"savedNumber"];
    [selectedDict setObject:title forKey:@"selected"];
    [muduleQuestions replaceObjectAtIndex:row withObject:selectedDict];
    
    [scoreLabel setText:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:NO]];
//    NSDictionary *lastDict= muduleQuestions.lastObject;
//    [lastDict setValue:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:YES] forKey:@"total"];
//    [muduleQuestions replaceObjectAtIndex:muduleQuestions.count-1 withObject:lastDict];
    NSMutableDictionary*seltedDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];

    [mainDict setObject:muduleQuestions forKey:@"rows"];
    [[DataManager sharedManager] saveDataToModul:mainDict modulNumber:[NSString stringWithFormat:@"%d",moduleSelected] folderName:[NSString stringWithFormat:@"%@",seltedDict[@"folderName"]]];

}


- (IBAction)nextButtonTap:(UIButton *)sender
{
    
    for (NSDictionary *dataDict in savedTestArray)
    {
        if ([dataDict[@"name"] length]==0)
        {
            [horizentolScrollView selectIndex:1];
            moduleSelected=1;

            return;
        }
    }

    update=NO;
    
    selectedTest=(int)savedTestArray.count+1;
    NSMutableDictionary*seltedDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-2]];

    [self saveDummyTest];
    
    [self loadTableViewFromPList];
    [horizentolScrollView selectIndex:1];
    moduleSelected=1;
}

- (IBAction)entfalltButtonTap:(UIButton *)sender
{
    [self.view endEditing:YES];
    [module5PopUp setHidden:YES];
    NSString *selectedtitle=@"entfällt";
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:module5PopUp.tag inSection:0];
    MCQOptionTableViewCell *cell = (MCQOptionTableViewCell *)[tblView cellForRowAtIndexPath:indexPath];
    cell.answeredLabel.text=selectedtitle;
    [cell.redImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor greenColor]]];
    
    int row=0;
    
    if (isSection0Cell0Expanded)
    {
        row=(int)indexPath.row-2;
    }
    else
    {
        row=(int)indexPath.row-1;
    }
    int scoreCalculated=0;
    
    NSMutableDictionary *dataDict=[NSMutableDictionary dictionary];
    [dataDict setObject:@"0" forKey:@"proTag"];
    [dataDict setObject:@"0" forKey:@"proWoche"];
    [dataDict setObject:@"0" forKey:@"proWoche"];
    [selectedDict setObject:dataDict forKey:@"savedValues"];

    [selectedDict setObject:[NSString stringWithFormat:@"%d",scoreCalculated] forKey:@"savedNumber"];
    [selectedDict setObject:selectedtitle forKey:@"selected"];
    [muduleQuestions replaceObjectAtIndex:row withObject:selectedDict];
    [scoreLabel setText:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:NO]];
//    NSDictionary *lastDict= muduleQuestions.lastObject;
//    [lastDict setValue:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:YES] forKey:@"total"];
//    [muduleQuestions replaceObjectAtIndex:muduleQuestions.count-1 withObject:lastDict];
    NSMutableDictionary*seltedDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];

    [mainDict setObject:muduleQuestions forKey:@"rows"];
    [[DataManager sharedManager] saveDataToModul:mainDict modulNumber:[NSString stringWithFormat:@"%d",moduleSelected]  folderName:[NSString stringWithFormat:@"%@",seltedDict[@"folderName"]]];

}
- (IBAction)werteButtonTap:(UIButton *)sender
{
    
    if (proTagTextfiled.text.length==0 && proWocheTextfiled.text.length==0 && proManatTextfiled.text.length==0 )
    {
        return;
    }
    
    [self.view endEditing:YES];
    [module5PopUp setHidden:YES];
    NSMutableDictionary *dataDict=[NSMutableDictionary dictionary];
    NSString *proTag=@"";
    NSString *proWoche=@"";
    NSString *proManat=@"";
    
    if (proTagTextfiled.text.length>0)
    {
        [dataDict setObject:proTagTextfiled.text forKey:@"proTag"];
        proTag=[NSString stringWithFormat:@"%@x pro Tag",proTagTextfiled.text];
    }
    else
    {
        [dataDict setObject:@"0" forKey:@"proTag"];
    }
    if (proWocheTextfiled.text.length>0)
    {
        [dataDict setObject:proWocheTextfiled.text forKey:@"proWoche"];
        proWoche=[NSString stringWithFormat:@"%@x pro Woche",proWocheTextfiled.text];

    }
    else
    {
        [dataDict setObject:@"0" forKey:@"proWoche"];
    }
    if (proManatTextfiled.text.length>0)
    {
        [dataDict setObject:proManatTextfiled.text forKey:@"proManat"];
        proManat=[NSString stringWithFormat:@"%@x pro Monat",proManatTextfiled.text];
    }
    else
    {
        [dataDict setObject:@"0" forKey:@"proManat"];
    }
    NSString *selectedtitle;
    
    if (proTagTextfiled.text.length>0)
    {
        selectedtitle=[NSString stringWithFormat:@"%@, %@, %@",proTag,proWoche,proManat];
    }
    else
    {
        selectedtitle=[NSString stringWithFormat:@"%@, %@",proWoche,proManat];
    }
    
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:module5PopUp.tag inSection:0];
    MCQOptionTableViewCell *cell = (MCQOptionTableViewCell *)[tblView cellForRowAtIndexPath:indexPath];
    cell.answeredLabel.text=selectedtitle;
    [cell.redImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor greenColor]]];
    
    int row=0;
    
    if (isSection0Cell0Expanded)
    {
        row=(int)indexPath.row-2;
    }
    else
    {
        row=(int)indexPath.row-1;
    }
    [selectedDict setObject:dataDict forKey:@"savedValues"];
    [selectedDict setObject:selectedtitle forKey:@"selected"];
    [muduleQuestions replaceObjectAtIndex:row withObject:selectedDict];

    int scoreCalculated=[[DataManager sharedManager] calculateMdule5Score:dataDict questionNumber:row+1 questionArray:muduleQuestions];
    NSLog(@"%d",scoreCalculated);

    
    [selectedDict setObject:[NSString stringWithFormat:@"%d",scoreCalculated] forKey:@"savedNumber"];
    [muduleQuestions replaceObjectAtIndex:row withObject:selectedDict];
    [scoreLabel setText:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:NO]];
//    NSDictionary *lastDict= muduleQuestions.lastObject;
//    [lastDict setValue:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:YES] forKey:@"total"];
//    [muduleQuestions replaceObjectAtIndex:muduleQuestions.count-1 withObject:lastDict];
    NSMutableDictionary*seltedDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];

    [mainDict setObject:muduleQuestions forKey:@"rows"];
    [[DataManager sharedManager] saveDataToModul:mainDict modulNumber:[NSString stringWithFormat:@"%d",moduleSelected]  folderName:[NSString stringWithFormat:@"%@",seltedDict[@"folderName"]]];

}
- (IBAction)selbstandigButtonTap:(UIButton *)sender
{
    [self.view endEditing:YES];
    [module5PopUp setHidden:YES];
    NSString *selectedtitle=@"selbständig";
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:module5PopUp.tag inSection:0];
    MCQOptionTableViewCell *cell = (MCQOptionTableViewCell *)[tblView cellForRowAtIndexPath:indexPath];
    cell.answeredLabel.text=selectedtitle;
    [cell.redImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor greenColor]]];
    
    int row=0;
    
    if (isSection0Cell0Expanded)
    {
        row=(int)indexPath.row-2;
    }
    else
    {
        row=(int)indexPath.row-1;
    }
    int scoreCalculated=0;
    
    NSMutableDictionary *dataDict=[NSMutableDictionary dictionary];
    [dataDict setObject:@"0" forKey:@"proTag"];
    [dataDict setObject:@"0" forKey:@"proWoche"];
    [dataDict setObject:@"0" forKey:@"proWoche"];
    [selectedDict setObject:dataDict forKey:@"savedValues"];

    [selectedDict setObject:[NSString stringWithFormat:@"%d",scoreCalculated] forKey:@"savedNumber"];
    [selectedDict setObject:selectedtitle forKey:@"selected"];
    [muduleQuestions replaceObjectAtIndex:row withObject:selectedDict];
    [scoreLabel setText:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:NO]];
//    NSDictionary *lastDict= muduleQuestions.lastObject;
//    [lastDict setValue:[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:YES] forKey:@"total"];
//    [muduleQuestions replaceObjectAtIndex:muduleQuestions.count-1 withObject:lastDict];
    NSMutableDictionary*seltedDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];

    [mainDict setObject:muduleQuestions forKey:@"rows"];
    [[DataManager sharedManager] saveDataToModul:mainDict modulNumber:[NSString stringWithFormat:@"%d",moduleSelected]  folderName:[NSString stringWithFormat:@"%@",seltedDict[@"folderName"]]];
}

- (IBAction)m1ButtonTap:(UIButton *)sender
{
    [horizentolScrollView selectIndex:1];
    moduleSelected=1;
}

- (IBAction)m2ButtonTap:(UIButton *)sender
{
    [horizentolScrollView selectIndex:2];
    moduleSelected=2;
}

- (IBAction)m4ButtonTap:(UIButton *)sender
{
    [horizentolScrollView selectIndex:4];
    moduleSelected=4;
}

- (IBAction)m5ButtonTap:(UIButton *)sender
{
    [horizentolScrollView selectIndex:5];
    moduleSelected=5;
}

- (IBAction)m6ButtonTap:(UIButton *)sender
{
    [horizentolScrollView selectIndex:6];
    moduleSelected=6;
}

- (IBAction)pdfButtonTap:(UIButton *)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSMutableDictionary *nameDict=[NSMutableDictionary dictionary];
        
        if (savedTestArray.count>selectedTest-1)
        {
            nameDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];
        }
        NSString*name=nameDict[@"name"];
        
        if (allComplete && name.length>0)
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd-MM-yyyy-HH:mm:SS"];
            
            NSDate *currentDate = [NSDate date];
            NSString *dateString = [formatter stringFromDate:currentDate];
            
            NSString *pdfName = dateString;
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",selectedTest] forKey:@"selectedTest"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.pdfOrderView = nil;
            
            self.pdfOrderView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PDFOrderView class]) owner:nil options:nil].firstObject;
            MAGPdfRenderer *renderer = [[MAGPdfRenderer alloc] init];
            renderer.delegate = self;
            renderer.pageInsets = (UIEdgeInsets){10, 20, 55, 20};
            renderer.printPageNumbers = YES;
            
            NSURL *pdfURL = [renderer drawView:self.pdfOrderView inPDFwithFileName:pdfName];
            
            [self previewPDFDocumentWithURL:pdfURL];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            });
            
        }
            else
            {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"NBI"
                                                               message: @"Bitte speichern sie ihre Eingaben vor dem erstellen einer PDF"
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
                [alert show];
            }
    });

    
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==proTagTextfiled)
    {
        [proTagImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor redColor]]];
        [proWocheImageView setBackgroundColor:[UIColor darkGrayColor]];
        [proManatImageView setBackgroundColor:[UIColor darkGrayColor]];
        
    }
    else if (textField==proWocheTextfiled)
    {
        [proWocheImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor redColor]]];
        [proTagImageView setBackgroundColor:[UIColor darkGrayColor]];
        [proManatImageView setBackgroundColor:[UIColor darkGrayColor]];

    }
    else if (textField==proManatTextfiled)
    {
        [proManatImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor redColor]]];
        [proTagImageView setBackgroundColor:[UIColor darkGrayColor]];
        [proWocheImageView setBackgroundColor:[UIColor darkGrayColor]];
    }
    else if (textField==nameTextField)
    {
        [nameImageView setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor redColor]]];
        [dobImageview setBackgroundColor:[UIColor darkGrayColor]];
    }
    else if (textField==dobTextField)
    {
        [dobImageview setBackgroundColor:[[Utils sharedInstance] darkerColorForColor:[UIColor redColor]]];
        [nameImageView setBackgroundColor:[UIColor darkGrayColor]];
    }
}
-(void)hideView
{
    [self.view endEditing:YES];
    [module5PopUp setHidden:YES];
    [saveName setHidden:YES];
    [clearAllView setHidden:YES];
}
-(void)calculateFinaleScore
{
    allComplete=YES;

    for (int i=1; i<7; i++)
    {
        moduleSelected=i;
        [self loadTableViewFromPList];
        if (i==1)
        {
            NSMutableDictionary *dataDict=muduleQuestions[muduleQuestions.count-2];
            if (![dataDict[@"selected"] isEqualToString:@"(unbeantwortet)"])
            {
                [Besonderer setText:dataDict[@"selected"]];
            }
        }
        
        NSString *totalNumberChoosen=[[DataManager sharedManager] calculateTheTotalNumberChoosen:muduleQuestions final:YES];

        NSString *finalScore=[[DataManager sharedManager] calculateScore:muduleQuestions moduleNumber:moduleSelected finalScore:YES];
        if ([finalScore isEqualToString:@"-"])
        {
            allComplete=NO;
        }
        else
        {
            NSDictionary *lastDict= muduleQuestions.lastObject;
            [lastDict setValue:finalScore forKey:@"total"];
            [lastDict setValue:totalNumberChoosen forKey:@"totalSelecetd"];
            
            [muduleQuestions replaceObjectAtIndex:muduleQuestions.count-1 withObject:lastDict];
            NSMutableDictionary*seltedDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];

            [mainDict setObject:muduleQuestions forKey:@"rows"];
            [[DataManager sharedManager] saveDataToModul:mainDict modulNumber:[NSString stringWithFormat:@"%d",moduleSelected]  folderName:[NSString stringWithFormat:@"%@",seltedDict[@"folderName"]]];
        }
        UIColor *resultColor;
        
        if ([finalScore isEqualToString:@"-"])
        {
            resultColor=[[Utils sharedInstance] darkerColorForColor:[UIColor redColor]];
        }
        else
        {
            resultColor=[[Utils sharedInstance] darkerColorForColor:[UIColor greenColor]];
        }

        if (i==1)
        {
            [finalScoreM1 setText:finalScore];
            [finamM1ImageView setBackgroundColor:resultColor];
        }
        else if (i==2)
        {
            [finalScoreM2 setText:finalScore];
            [finamM12mageView setBackgroundColor:resultColor];
        }
        else if (i==3)
        {
            if ([finalScore doubleValue]>[finalScoreM2.text doubleValue])
            {
                [finalScoreM2 setText:finalScore];
            }
            [finamM12mageView setBackgroundColor:resultColor];
        }
        else if (i==4)
        {
            [finalScoreM4 setText:finalScore];
            [finamM14mageView setBackgroundColor:resultColor];
        }
        else if (i==5)
        {
            [finalScoreM5 setText:finalScore];
            [finamM15mageView setBackgroundColor:resultColor];
        }
        else if (i==6)
        {
            [finalScoreM6 setText:finalScore];
            [finamM16mageView setBackgroundColor:resultColor];
        }
    }
    NSMutableDictionary *nameDict=[NSMutableDictionary dictionary];
    if (savedTestArray.count>selectedTest-1)
    {
        nameDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];
    }
    

    if (allComplete)
    {
        completeTotal.text = [self calculateFinalTotal];
        pdfLabel.text = [[DataManager sharedManager] finalPdfScore:[completeTotal.text doubleValue]];
        if ([Besonderer.text isEqualToString:@"Ja"])
        {
            pdfLabel.text = @"PG 5";
        }
        [nameDict setObject:@"1" forKey:@"complete"];
        NSString *newStr = [pdfLabel.text substringFromIndex:3];
        [nameDict setObject:newStr forKey:@"status"];
    }
    else
    {
        pdfLabel.text=@"-";
        completeTotal.text=@"-";
        [nameDict setObject:@"0" forKey:@"complete"];
        [nameDict setObject:@"-" forKey:@"status"];
    }
    if (savedTestArray.count>selectedTest-1)
    {
        resultDict=[NSMutableDictionary dictionary];
        [savedTestArray replaceObjectAtIndex:selectedTest-1 withObject:nameDict];
        [[DataManager sharedManager] saveTest:savedTestArray];
    }
    else
    {
        resultDict=[NSMutableDictionary dictionaryWithDictionary:nameDict];
    }
}

-(NSString*)calculateFinalTotal
{
    return [NSString stringWithFormat:@"%.02f",([finalScoreM1.text doubleValue]+[finalScoreM2.text doubleValue]+[finalScoreM4.text doubleValue]+[finalScoreM5.text doubleValue]+[finalScoreM6.text doubleValue])];
}

#pragma mark - Auxiliaries

- (void)previewPDFDocumentWithURL:(NSURL *)pdfURL {
    UIDocumentInteractionController *documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:pdfURL];
    documentInteractionController.delegate = self;
    [documentInteractionController presentPreviewAnimated:YES];
}
- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
    
}
- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

}

#pragma mark - UIDocumentInteractionControllerDelegate implementation

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

#pragma mark - MAGPdfRendererDelegate implementation

- (NSArray<UIView *> *)noWrapViewsForPdfRenderer:(MAGPdfRenderer *)pdfRenderer {
    return self.pdfOrderView.noWrapViews;
}

- (IBAction)eraseData:(UIButton *)sender
{
    if (savedTestArray.count>selectedTest-1)
    {
        [deleteButton setEnabled:YES];
    }
    else
    {
        [deleteButton setEnabled:NO];
    }
    [clearAllView setHidden:NO];
    [self.view bringSubviewToFront:clearAllView];
}
- (IBAction)refershAllPlistsButtonTap:(UIButton *)sender
{
   LGAlertView*alert= [[LGAlertView alloc] initWithTitle:@"Möchtest du die nicht gespeicherten Änderungen verwerfen?" message:@"" style:LGAlertViewStyleAlert buttonTitles:@[@"Ja", @"Nein"] cancelButtonTitle:nil destructiveButtonTitle:nil actionHandler:^(LGAlertView * _Nonnull alertView, NSUInteger index, NSString * _Nullable title){
        if (index==0)
        {
            update=NO;

            selectedTest=(int)savedTestArray.count+1;
            [self saveDummyTest];
//            [[DataManager sharedManager] eraseDataPlistsToDocumentDirectories];
            [self loadTableViewFromPList];
        }
        else
        {
            
        }
        [clearAllView setHidden:YES];

    } cancelHandler:^(LGAlertView * _Nonnull alertView) {
        
    } destructiveHandler:^(LGAlertView * _Nonnull alertView) {
        
    }];
    [alert show];
}

- (IBAction)löschenButtonTap:(UIButton *)sender
{
    [clearAllView setHidden:YES];
    // Delete
    NSMutableDictionary *nameDict=[NSMutableDictionary dictionary];
    
    if (savedTestArray.count>selectedTest-1)
    {
        nameDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];
    }
    NSString *message=[NSString stringWithFormat:@"Möchtest du die Begutachtung von %@ wirklich löschen?",nameDict[@"name"]];
                   
    
    LGAlertView*alert= [[LGAlertView alloc] initWithTitle:message message:@"" style:LGAlertViewStyleAlert buttonTitles:@[@"Ja", @"Nein"] cancelButtonTitle:nil destructiveButtonTitle:nil actionHandler:^(LGAlertView * _Nonnull alertView, NSUInteger index, NSString * _Nullable title){
        if (index==0)
        {
            update=YES;
            NSMutableDictionary*delDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];
            [[DataManager sharedManager] removeTestFolder:delDict[@"folderName"]];
            
            [savedTestArray removeObjectAtIndex:selectedTest-1];
            [[DataManager sharedManager] saveTest:savedTestArray];
            [saveTestTableView reloadData];

            if (savedTestArray.count>0)
            {
                selectedTest=(int)savedTestArray.count;
            }
            else
            {
                [self saveDummyTest];
                selectedTest=1;
                update=NO;
            }
            [self loadTableViewFromPList];
            [horizentolScrollView selectIndex:0];
            moduleSelected=0;

        }
        else
        {
            
        }
        [clearAllView setHidden:YES];
        
    } cancelHandler:^(LGAlertView * _Nonnull alertView) {
        
    } destructiveHandler:^(LGAlertView * _Nonnull alertView) {
        
    }];
    [alert show];

}

- (IBAction)saveButtonTap:(UIButton *)sender
{
    if (update)
    {
        NSMutableDictionary *dataDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];
        nameTextField.text=dataDict[@"name"];
        dobTextField.text=dataDict[@"dob"];
    }
    else
    {
        nameTextField.text=@"";
        dobTextField.text=@"";
    }
    [saveName setHidden:NO];
    [self.view bringSubviewToFront:saveName];

}

- (IBAction)saveNameButtonTap:(UIButton *)sender
{
    if (nameTextField.text.length>0 && dobTextField.text.length>0)
    {
        [self.view endEditing:YES];
        NSMutableDictionary *nameDict=[NSMutableDictionary dictionary];
        nameDict=[NSMutableDictionary dictionaryWithDictionary:savedTestArray[selectedTest-1]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        
        NSDate *currentDate = [NSDate date];
        NSString *dateString = [formatter stringFromDate:currentDate];

        if (update)
        {
        }
        else
        {

            [nameDict setObject:@"0" forKey:@"complete"];
            [nameDict setObject:@"-" forKey:@"status"];
            [nameDict setObject:dateString forKey:@"date"];
//            [nameDict setObject:[NSString stringWithFormat:@"%d",selectedTest] forKey:@"folderName"];
        }
        if ([resultDict[@"status"] length]>0)
        {
            [nameDict setObject:resultDict[@"complete"] forKey:@"complete"];
            [nameDict setObject:resultDict[@"status"] forKey:@"status"];
        }
        [nameDict setObject:nameTextField.text forKey:@"name"];
        [nameDict setObject:dobTextField.text forKey:@"dob"];
        [nameDict setObject:dateString forKey:@"date"];

        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0];
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"SavedTest.plist"]];
        
        // Build the array from the plist
        savedTestArray=[NSMutableArray arrayWithContentsOfFile:plistPath];
        NSArray *oldArray=[NSArray arrayWithArray:savedTestArray];
        
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:NO];
        savedTestArray=[NSMutableArray arrayWithArray:[oldArray sortedArrayUsingDescriptors:@[sort]]];

//        if (update)
        {
            [savedTestArray removeObjectAtIndex:selectedTest-1];
        }
        [savedTestArray addObject:nameDict];
        
        if ([nameDict[@"name"] length]>0)
        {
            [titleLabel setText:[NSString stringWithFormat:@"NBI - %@",nameDict[@"name"]]];
        }
        else
        {
            [titleLabel setText:@"NBI - Neue Begutachtung"];
        }

        
        [[DataManager sharedManager] saveTest:savedTestArray];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            savedTestArray=[NSMutableArray arrayWithContentsOfFile:plistPath];
            NSArray *oldArray=[NSArray arrayWithArray:savedTestArray];
            
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:NO];
            savedTestArray=[NSMutableArray arrayWithArray:[oldArray sortedArrayUsingDescriptors:@[sort]]];

            [saveTestTableView reloadData];
        });

        update=YES;
        
        [saveName setHidden:YES];
    }
}
-(void)saveDummyTest
{
    NSMutableDictionary *nameDict=[NSMutableDictionary dictionary];
    
    [nameDict setObject:@"0" forKey:@"complete"];
    [nameDict setObject:@"-" forKey:@"status"];
    [nameDict setObject:@"" forKey:@"date"];
    [nameDict setObject:@"" forKey:@"name"];
    [nameDict setObject:@"" forKey:@"dob"];
    
    if (savedTestArray.count>0)
    {
        int foldername=[self maxFolderName]+1;
        [nameDict setObject:[NSString stringWithFormat:@"%d",foldername] forKey:@"folderName"];
    }
    else
    {
        [nameDict setObject:[NSString stringWithFormat:@"%d",selectedTest] forKey:@"folderName"];
    }
    
    [[DataManager sharedManager] movePlistsToDocumentDirectories:[NSString stringWithFormat:@"%@",nameDict[@"folderName"]]];

    [savedTestArray addObject:nameDict];
    [saveTestTableView reloadData];
    
    [[DataManager sharedManager] saveTest:savedTestArray];

}
-(int)maxFolderName
{
    int folder=0;
    for (NSMutableDictionary*dataDict in savedTestArray)
    {
        if ([dataDict[@"folderName"] intValue]>folder)
        {
            folder=[dataDict[@"folderName"] intValue];
        }
    }
    return folder;
}
@end
