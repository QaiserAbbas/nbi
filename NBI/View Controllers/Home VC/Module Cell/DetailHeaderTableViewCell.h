//
//  DetailHeaderTableViewCell.h
//  NBI
//
//  Created by Qaiser Abbas on 2/19/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *expandImageView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;

@end
