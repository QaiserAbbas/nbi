//
//  MCQOptionTableViewCell.h
//  NBI
//
//  Created by Qaiser Abbas on 2/19/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCQOptionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answeredLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *redImageView;

@end
