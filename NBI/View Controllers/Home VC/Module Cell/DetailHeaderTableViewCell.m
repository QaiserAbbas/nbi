//
//  DetailHeaderTableViewCell.m
//  NBI
//
//  Created by Qaiser Abbas on 2/19/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import "DetailHeaderTableViewCell.h"

@implementation DetailHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
