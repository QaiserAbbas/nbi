//
//  DetailTableViewCell.h
//  NBI
//
//  Created by Qaiser Abbas on 2/19/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
