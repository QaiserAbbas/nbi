//
//  AppDelegate.h
//  NBI
//
//  Created by Qaiser Abbas on 2/15/18.
//  Copyright © 2018 Qaiser Abbas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimatorSlide.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UIStoryboard* storyboard;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

